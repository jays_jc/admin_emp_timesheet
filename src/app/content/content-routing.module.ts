import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListContentComponent } from './list-content/list-content.component';
import { ViewContentComponent } from './view-content/view-content.component';
import { AddUpdateContentComponent } from './add-update-content/add-update-content.component';

const routes: Routes = [
  {
    path: '',
    children: [
     {
        path: '',
        component: ListContentComponent
      },
      {
        path: 'list',
        component: ListContentComponent
      },
      {
        path: 'list/:slug',
        component: ViewContentComponent
      },
      {
        path: 'edit/:slug',
        component: AddUpdateContentComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule { }
