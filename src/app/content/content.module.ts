import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentService } from './content.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CKEditorModule } from 'ng2-ckeditor';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ContentRoutingModule } from './content-routing.module';
import { ListContentComponent } from './list-content/list-content.component';
import { ViewContentComponent } from './view-content/view-content.component';
import { AddUpdateContentComponent } from './add-update-content/add-update-content.component';

@NgModule({
  declarations: [ListContentComponent, ViewContentComponent, AddUpdateContentComponent],
  imports: [
    CommonModule,
    ContentRoutingModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    CKEditorModule,
    NgxDatatableModule
  ],
  providers: [ContentService]
})
export class ContentModule { }
