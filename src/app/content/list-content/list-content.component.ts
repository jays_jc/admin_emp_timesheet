import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ContentService } from '../content.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-content',
  templateUrl: './list-content.component.html',
  styleUrls: ['./list-content.component.css']
})
export class ListContentComponent implements OnInit {

  public contentData: Array<any> = [];
  public response: any;

  rows = [];
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 1;
  filters: { page: number; search: string } = { page: 1, search: '' };

  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private contService: ContentService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getContents();
  }

  edit(ID) {
    let route = '/content/edit/' + ID;
    this.router.navigate([route]);
  }

  view(ID) {
    let route = '/content/list/' + ID;
    this.router.navigate([route]);
  }

  getContents() {
    this.isLoading = true;
    this._subscriberData = this.contService.getAllContent(this.filters).subscribe((response) => {
      if (response.data.length == 0) {
        this.contentData = [];
        this.isLoading = false;
      } else {
        this.contentData = response.data.map(cat => {
          return {
            id: cat.id,
            title: cat.title,
            date: cat.createdAt,
            status: cat.status,
            slug: cat.slug
          }
        });
        this.isLoading = false;
      }
    });
  }

  changeStatus(data, ID) {
    let Status = '';
    let message = 'Do you want to activate this content ?';
    if (data == 'active') {
      Status = "deactive";
      message = 'Do you want to deactivate this content ?';
    } else {
      Status = "active";
    }


    if (confirm(message)) {
      this.spinner.show();
      this.contService.status(ID, 'contentmanagment', Status).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success(res.data.message);
          this.getContents();
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    this.getContents();
  }

  searchValue() {
    this.page = 1;
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getContents();
  }

  clearValue() {
    this.page = 1;
    this.filters.search = '';
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getContents();
  }



}
