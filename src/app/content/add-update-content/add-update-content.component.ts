  import { Component, OnInit, ViewChild } from '@angular/core';
  import { Router, ActivatedRoute } from '@angular/router';
  import { FormGroup, FormBuilder, Validators } from '@angular/forms';
  import { NgxSpinnerService } from 'ngx-spinner';
  import { ContentService } from '../content.service';
  import { ToastrService } from 'ngx-toastr';
  import { environment } from '../../../environments/environment';
  
  @Component({
    selector: 'app-add-update-content',
    templateUrl: './add-update-content.component.html',
    styleUrls: ['./add-update-content.component.css']
  })
  export class AddUpdateContentComponent implements OnInit {
    
    public contentID: any;
    public response: any;
    conditionalForm: boolean = false;
    public _host = environment.url;
  
    public contentForm: FormGroup;
    submitted = false;
    _contObservable: any;
  
    constructor(
      private router: Router,
      private _activateRouter: ActivatedRoute,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService,
      private contService: ContentService,
      private formBuilder: FormBuilder,
    ) {
      this.createForm();
    }
  
    ngOnInit(): void {  
      this.contentID = this._activateRouter.snapshot.params['slug'];
  
      if (this.contentID) {
        this.conditionalForm = true;
        this.spinner.show();
        this.contService.get(this.contentID).subscribe((res: any) => {
          if (res.code == 200) {
            this.contentForm.patchValue({
              title: res.data.title,
              description: res.data.description,
              meta_title: res.data.meta_title,
              meta_description: res.data.meta_description
            });
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          });
      } else {
        this.conditionalForm = false;
      }
  
    }
  
    createForm() {
      this.contentForm = this.formBuilder.group({
        title: ['', Validators.required],
        meta_title: ['', Validators.required],
        meta_description: ['', Validators.required],
        description: ['', Validators.required]
      });
    }
  
    get f() { return this.contentForm.controls; }
  
  
    onSubmit() {
      this.submitted = true;
      if (!this.contentForm.invalid) {
        this.spinner.show();
  
        if (this.conditionalForm) {
          this._contObservable = this.contService.update(this.contentForm.value, this.contentID).subscribe(res => {
            let url;
            if (res.code == 200) {
              this.toastr.success('Content updated successfully!');
              url = '/content';
              this.router.navigate([url]);
            } else {
              this.toastr.error(res.error.message, 'Error');
            }
            this.spinner.hide();
          },
            error => {
              this.spinner.hide();
              // this.toastr.error(error, 'Error');
            });
  
        } else {
          this._contObservable = this.contService.add(this.contentForm.value).subscribe(res => {
            let url;
            if (res.code == 200) {
              this.toastr.success('Content added successfully!');
              url = '/content';
              this.router.navigate([url]);
            } else {
              this.toastr.error(res.error.message, 'Error');
            }
            this.spinner.hide();
          },
            error => {
              this.spinner.hide();
              // this.toastr.error(error, 'Error');
            });
        }
      }
    }
  
  
    ngOnDestroy(): void {
      if (this._contObservable) {
        this._contObservable.unsubscribe();
      }
    }
  
  
  
  
  }
