  import { Component, OnInit } from '@angular/core';
  import { Router, ActivatedRoute } from '@angular/router';
  import { NgxSpinnerService } from 'ngx-spinner';
  import { ContentService } from '../content.service';
  import { ToastrService } from 'ngx-toastr';
  import { environment } from '../../../environments/environment';

  @Component({
    selector: 'app-view-content',
    templateUrl: './view-content.component.html',
    styleUrls: ['./view-content.component.css']
  })
  export class ViewContentComponent implements OnInit {

    public _host = environment.url;

    public contentID: any;
    public content: any = {};
    conditionalForm: boolean = false;

    constructor(
      private router : Router,
      private _activateRouter: ActivatedRoute,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService,
      private contService: ContentService ) { }
  
    ngOnInit(): void {
      this.contentID = this._activateRouter.snapshot.params['slug']; 
      
      if(this.contentID){
        this.conditionalForm = true;
        this.spinner.show();
        this.contService.get(this.contentID).subscribe((res: any) => {
          if (res.success) {
            this.content = res.data;
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
      }else{
       this.conditionalForm = false;
      }
    }

  
  
  
  }
