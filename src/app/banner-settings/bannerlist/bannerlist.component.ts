import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ToastrService } from 'ngx-toastr';
import { BannerService } from '../banner.service';

@Component({
  selector: 'app-bannerlist',
  templateUrl: './bannerlist.component.html',
  styleUrls: ['./bannerlist.component.scss']
})
export class BannerlistComponent implements OnInit {

  public bannerData: Array<any> = [];
  public response: any;
  public response2: any;

  rows = [];
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; search: string } = { page: 2, search: '' };

  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private bannerService: BannerService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router
  ) { }


  ngOnInit(): void {
    this.getBanner();
  }

  edit(ID) {
    let route = '/banner/edit/' + ID;
    this.router.navigate([route]);
  }

  view(ID) {
    let route = '/banner/list/' + ID;
    this.router.navigate([route]);
  }

  /* Function use to remove Crop with crop id */
  remove(ID) {
    if (confirm("Do you want to delete this Banner ?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'banner'
      }

      this.bannerService.deleteRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Banner Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getBanner();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  getBanner() {
    this.isLoading = true;
    this._subscriberData = this.bannerService.getAllBanner(this.filters).subscribe((response) => {
      if (response.banners.length == 0) {
        this.bannerData = [];
        this.isLoading = false;
      } else {
        this.bannerData = response.banners;
        this.isLoading = false;
      }
    });
  }

  changeStatus(data, ID) {
    let Status = '';
    let message = 'Do you want to activate this blog ?';
    if (data == 'active') {
      Status = "deactive";
      message = 'Do you want to deactivate this blog ?';
    } else {
      Status = "active";
    }


    if (confirm(message)) {
      this.spinner.show();
      this.bannerService.status(ID, 'blogs', Status).subscribe((res: any) => {
        if (res.success) {
          this.response2 = res;
          this.toastr.success(res.data.message);
          this.getBanner();
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  setPage(e) {
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    this.getBanner();
  }

  searchValue() {
    this.page = 0;
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getBanner();
  }

  clearValue() {
    this.page = 0;
    this.filters.search = '';
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getBanner();
  }

}
