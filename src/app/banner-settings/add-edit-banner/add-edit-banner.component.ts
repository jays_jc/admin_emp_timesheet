import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ImageResult, ResizeOptions } from 'ng2-imageupload';
import { environment } from '../../../environments/environment';
import { BannerService } from '../banner.service';

@Component({
  selector: 'app-add-edit-banner',
  templateUrl: './add-edit-banner.component.html',
  styleUrls: ['./add-edit-banner.component.scss']
})
export class AddEditBannerComponent implements OnInit {

  @ViewChild('myInput', { static: false })
  myInputVariable: any;

  public ID: any;
  public blogImage = '';
  public token = '';
  public types: any = [];
  public images: any = [];
  public response: any;
  public afuConfig: any = {};
  public data: any = {};
  conditionalForm: boolean = false;
  public _host = environment.url;

  public bannerForm: FormGroup;
  submitted = false;
  _blogObservable: any;

  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private bannerService: BannerService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,) {
    this.createForm();
  }

  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.ID = this._activateRouter.snapshot.params['id'];

    if (this.ID) {
      this.conditionalForm = true;
      this.spinner.show();
      this.bannerService.get(this.ID).subscribe((res: any) => {
        if (res.code == 200) {
          this.bannerForm.patchValue({
            title: res.banner.title,
            description: res.banner.description,
            refrence: res.banner.refrence,
            image: res.banner.image,
            shop_now: res.banner.shop_now
          });
          this.blogImage = res.banner.image;
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
    } else {
      this.conditionalForm = false;
    }
  }


  createForm() {
    this.bannerForm = this.formBuilder.group({
      title: [''],
      description: [''],
      shop_now: [false],
      refrence: [''],
      image: ['', Validators.required]
    });
  }

  get f() { return this.bannerForm.controls; }

  shopchange() {
    if (this.bannerForm.value.shop_now) {
      this.bannerForm.get('refrence').setValidators([Validators.required]);
      this.bannerForm.controls['refrence'].updateValueAndValidity();
    }
    else {
      this.bannerForm.get('refrence').setValidators([]);
      this.bannerForm.controls['refrence'].updateValueAndValidity();
    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.bannerForm.controls.image.value == '') {
      this.toastr.warning('Please upload banner image');
    }



    if (!this.bannerForm.invalid) {
      this.spinner.show();

      if (this.conditionalForm) {
        this._blogObservable = this.bannerService.update(this.bannerForm.value, this.ID).subscribe(res => {
          let url;
          if (res.code == 200) {
            this.toastr.success('Banner updated successfully!');
            url = '/banner';
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          });

      } else {
        this._blogObservable = this.bannerService.add(this.bannerForm.value).subscribe(res => {
          let url;
          if (res.code == 200) {
            this.toastr.success('Banner added successfully!');
            url = '/banner';
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          });
      }
    }
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'banner'
    }
    this.myInputVariable.nativeElement.value = "";
    this.spinner.show();
    this.bannerService.uploadImage(object).subscribe((res: any) => {
      if (res.success) {
        this.blogImage = res.data.fullPath;
        this.bannerForm.controls.image.setValue(this.blogImage);
      } else {
        window.scrollTo(0, 0);
        this.toastr.error(res.error.message, 'Error');
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastr.error('There are some errors, please try again after some time !', 'Error');
    });
  }

  removeImage() {
    this.bannerForm.controls.image.setValue('');
    this.blogImage = '';
  }

  ngOnDestroy(): void {
    if (this._blogObservable) {
      this._blogObservable.unsubscribe();
    }
  }

}
