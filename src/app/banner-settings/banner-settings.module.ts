import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEditBannerComponent } from './add-edit-banner/add-edit-banner.component';
import { BannerlistComponent } from './bannerlist/bannerlist.component';
import { BannersRoutingModule } from './banners-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CKEditorModule } from 'ng2-ckeditor';
import { AuthInterceptor } from '../shared/auth-interceptor';
import { ImageUploadModule } from 'ng2-imageupload';
import { BannerService } from './banner.service';
import { ViewBannerComponent } from './view-banner/view-banner.component';

@NgModule({
  declarations: [AddEditBannerComponent, BannerlistComponent, ViewBannerComponent],
  imports: [
    CommonModule,
    BannersRoutingModule,
    CommonModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
    ImageUploadModule,
    CKEditorModule,
  ],
  providers: [
    BannerService,
    {provide:HTTP_INTERCEPTORS ,useClass:AuthInterceptor, multi:true}
  ],
})
export class BannerSettingsModule { }
