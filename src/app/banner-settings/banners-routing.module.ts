import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEditBannerComponent } from './add-edit-banner/add-edit-banner.component';
import { BannerlistComponent } from './bannerlist/bannerlist.component';
import { ViewBannerComponent } from './view-banner/view-banner.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: BannerlistComponent
      },
      {
        path: 'list',
        component: BannerlistComponent
      },
      {
        path: 'add',
        component: AddEditBannerComponent
      },
      {
        path: 'list/:id',
        component: ViewBannerComponent
      },
      {
        path: 'edit/:id',
        component: AddEditBannerComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BannersRoutingModule { }
