import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { BannerService } from '../banner.service';

@Component({
  selector: 'app-view-banner',
  templateUrl: './view-banner.component.html',
  styleUrls: ['./view-banner.component.scss']
})
export class ViewBannerComponent implements OnInit {

  public _host = environment.url
    public ID: any;
    public banner: any = {};
    conditionalForm: boolean = false;
    public popupImage = '';

  constructor(private router : Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private bannerService: BannerService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.ID = this._activateRouter.snapshot.params['id']; 
      
      if(this.ID){
        this.conditionalForm = true;
        this.spinner.show();
        this.bannerService.get(this.ID).subscribe((res: any) => {
          if (res.success) {
            this.banner = res.banner;
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
      }else{
       this.conditionalForm = false;
      }
  }

  openModal(img) {
    this.popupImage = img;
  }


}
