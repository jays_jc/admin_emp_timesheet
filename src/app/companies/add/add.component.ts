import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CompaniesService } from '../companies.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  pageNo: any;
  public companyId: any;
  public response: any;
  conditionalForm: boolean = false;

  public companyForm: FormGroup;
  submitted = false;
  _userObservable: any;

  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private companiesService: CompaniesService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    // this.getRoles();

    this.companyId = this._activateRouter.snapshot.params['id'];
    this.pageNo = this._activateRouter.snapshot.params['page'];

    if (this.companyId) {
      this.conditionalForm = true;
      this.spinner.show();
      this.companiesService.getCompany(this.companyId).subscribe((res: any) => {
        if (res.success) {
          this.companyForm.patchValue({
            name: res.data.name,
            address: res.data.address,
            email: res.data.email,
            mobileNo: res.data.mobileNo,
            billing_type:res.data.billing_type
          });
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
    } else {
      this.conditionalForm = false;
    }

  }

  createForm() {
    this.companyForm = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mobileNo: ['', [Validators.required, Validators.min(1000000000), Validators.max(999999999999), Validators.pattern]],
      billing_type: ['', Validators.required]
    });
    this.companyForm.patchValue({
      billing_type:"non_billable"
    })
  }

  get f() { return this.companyForm.controls; }


  // getRoles() {
  //   this.userService.getAllRoles().subscribe((response) => {
  //     if (response.data.length == 0) {
  //       this.roleData = [];
  //     } else {
  //       this.roles = response.data;
  //       this.roles.forEach(element => {
  //         if (element.name == "employer" || element.name == "sub_admin") {
  //           this.roleData.push(element)
  //         }
  //       });
  //     }
  //   });
  // }

  onSubmit() {
    this.submitted = true;
    if (!this.validateEmail(this.companyForm.value.email)) {
      this.toastr.warning('Please enter a valid email.');
      return;
    }
    if (!this.companyForm.invalid) {
      this.spinner.show();
      let data = this.companyForm.value;
      if(this.companyId){
        data['id'] = this.companyId
      }
     

      if (this.conditionalForm) {
        this._userObservable = this.companiesService.update(data).subscribe(res => {
          let url;
          if (res.success) {
            this.toastr.success('Companies Updated Successfully!');
            url = '/companies/companies/' + this.pageNo;
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          });

      } else {
        this._userObservable = this.companiesService.add(this.companyForm.value).subscribe(res => {
          let url;
          if (res.success) {
            this.toastr.success('Companies Added Successfully!');
            url = '/companies';
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          }
        );
      }
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  ngOnDestroy(): void {
    if (this._userObservable) {
      this._userObservable.unsubscribe();
    }
  }




}



