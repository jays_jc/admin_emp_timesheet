// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';

// import { CompaniesRoutingModule } from './companies-routing.module';
// import { AddComponent } from './add/add.component';
// import { ListComponent } from './list/list.component';
// import { ViewComponent } from './view/view.component';


// @NgModule({
//   declarations: [AddComponent, ListComponent, ViewComponent],
//   imports: [
//     CommonModule,
//     CompaniesRoutingModule
//   ]
// })
// export class CompaniesModule { }
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';;
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { CompaniesService } from './companies.service';
import { AddComponent } from './add/add.component';
import { CompaniesRoutingModule } from './companies-routing.module';


@NgModule({
  declarations: [AddComponent, ListComponent, ViewComponent],
  imports: [
    CommonModule,
    CompaniesRoutingModule,
    NgxSpinnerModule,
    NgxDatatableModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [CompaniesService]
})
export class CompaniesModule { }

