import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ActivatedRoute, Router } from '@angular/router';
import { CompaniesService } from '../companies.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  actionType: any = 'active'
  public userData: Array<any> = [];
  public response: any;
  public response2: any;

  totalItems=0

  rows = [];
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  role: any = 'company';
  isLoading: boolean = false;
  page = 1;
  isDeleted = false
  filters: { page: number; search: string, isDeleted: boolean } = { page: this.page, search: '', isDeleted: false };

  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private companiesService: CompaniesService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router,
    private _activateRouter: ActivatedRoute,
  ) {


    this.companiesService.actionType.subscribe(res => {
      if (res !== '' && res !== undefined && res !== null) {
        this.actionType = res
        if (this.actionType == 'active') {
          this.filters.isDeleted = false;

        } else if (this.actionType == 'deleted') {
          this.filters.isDeleted = true;
        }
      } else {
        this.actionType = 'active'
      }
    })
  }

  ngOnInit(): void {
    if (this._activateRouter.snapshot.params['page']) {

      this.filters.page = JSON.parse(this._activateRouter.snapshot.params['page']);
      this.page = this.filters.page;
      console.log(this.filters.page, typeof (this._activateRouter.snapshot.params['page']))
      Object.assign(this.filters, { page: this.filters.page });
      this.getAllCompanies();
    } else {
      this.page = 1
      this.getAllCompanies();
    }

  }
  filterData(value) {
    this.role = "";
    if (value === "" || value === "all") {
      delete this.filters['role'];
      this.getAllCompanies();
    } else {
      this.role = value
      Object.assign(this.filters, { role: value });
      // this.role = value;  
      this.getAllCompanies();
    }
    console.log('this.role', this.role)
  }
  viewUser(companyID, type) {
    this.companiesService.actionType.next(type)
    let route = '/companies/list/' + companyID + '/' + this.page;
    this.router.navigate([route]);
  }
  add() {
    let route = '/companies/add/';
    this.companiesService.actionType.next('active')
    this.router.navigate([route]);
  }
  edit(ID, type) {
    this.companiesService.actionType.next(type)
    this.companiesService.activePage.next(this.page)
    let route = '/companies/edit/' + ID + '/' + this.page;
    this.router.navigate([route]);
  }

  /* Function use to remove Crop with crop id */
  remove(ID) {
    if (confirm("Do you want to delete this company?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'users'
      }

      this.companiesService.deleteRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Company Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getAllCompanies();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  /* Function use to remove Crop with crop id */
  moveBack(ID) {
    if (confirm("Do you want to move back this company to listing ?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'users'
      }

      this.companiesService.moveBackRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Company Moved Back Successfully');
        } else {
          this.toastr.error('Unable to move at the moment, Please try again later', 'Error');
        }
        this.getAllCompanies();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  getAllCompanies() {
    this.spinner.show();
    this.isLoading = true;
    if (this.role) {
      Object.assign(this.filters, { role: this.role });
    }
    this._subscriberData = this.companiesService.getAllCompanies(this.filters).subscribe((response) => {
      if (response.data.length == 0) {
        this.userData = [];
        this.totalItems = response.total;
        this.isLoading = false;
        this.spinner.hide();
      } else {
        this.userData = response.data.map(data => {
          return {
            id: data._id,
            name: data.name,
            email: data.email,
            address: data.address,
            date: data.createdAt,
            status: data.status,
            deletedBy: data.deletedBy,
            deletedAt: data.deletedAt
          }
        });
        this.totalItems = response.total;
        this.isLoading = false;
        this.spinner.hide();
      }
    });
  }

  /* Function use to remove Crop with crop id */
  removePermanent(ID) {
    if (confirm("Do you want to delete this company permanently?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'users'
      }

      this.companiesService.deleteRecordPermanent(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Company Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getAllCompanies();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  changeStatus(data, ID) {
    let Status = '';
    let message = 'Do you want to activate this company ?';
    if (data == 'active') {
      Status = "deactive";
      message = 'Do you want to deactivate this company  ?';
    } else {
      Status = "active";
    }


    if (confirm(message)) {
      this.spinner.show();
      this.companiesService.status(ID, 'users', Status).subscribe((res: any) => {
        if (res.success) {
          this.response2 = res;
          this.toastr.success(res.message);
          this.getAllCompanies();
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    let route = '/companies/companies/' + this.page;
    this.router.navigate([route]);
    this.getAllCompanies();
  }

  searchValue() {
    this.page = 1;
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getAllCompanies();
  }

  clearValue() {
    this.page = 1;
    this.filters.search = '';
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getAllCompanies();
  }



}


