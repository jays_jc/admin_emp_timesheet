import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';


const routes: Routes = [ {
  path: '',
  data: {
    title: 'Companies'
  },
  children: [
    {
      path: '',
      component: ListComponent
    },{
      path: 'companies/:page',
      component: ListComponent
    },
    {
      path: 'list',
      component: ListComponent
    },
    {
      path: 'list/:id/:page',
      component: ViewComponent
    },
    {
      path: 'add',
      component: AddComponent
    },
    {
      path: 'edit/:id',
      component: AddComponent
    },{
      path: 'edit/:id/:page',
      component: AddComponent
    }
  ]
}
  ]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompaniesRoutingModule { }
