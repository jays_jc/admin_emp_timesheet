import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RolesService } from './roles.service';

import { RolesPermissionsRoutingModule } from './roles-permissions-routing.module';
import { AddUpdateRolesComponent } from './add-update-roles/add-update-roles.component';
import { ListRolesComponent } from './list-roles/list-roles.component';


@NgModule({
  declarations: [AddUpdateRolesComponent, ListRolesComponent],
  imports: [
    CommonModule,
    RolesPermissionsRoutingModule,
    NgxSpinnerModule,
    NgxDatatableModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [RolesService]
})
export class RolesPermissionsModule { }
