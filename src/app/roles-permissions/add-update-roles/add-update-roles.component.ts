import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormArray, FormControl, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { RolesService } from '../roles.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-update-roles',
  templateUrl: './add-update-roles.component.html',
  styleUrls: ['./add-update-roles.component.scss']
})
export class AddUpdateRolesComponent implements OnInit {
  pageNo: any;
  public roleID: any;
  public response: any;
  conditionalForm: boolean = false;

  public roleForm: FormGroup;
  submitted = false;
  _roleObservable: any;

  rolePermissions = [
    {
      label: 'Users Management',
      name: 'usr_mgnt',
      isChecked: false
    },
    {
      label: 'View Users',
      name: 'view_user',
      isChecked: false
    },
    {
      label: 'blog Management',
      name: 'blog_mgmt',
      isChecked: false
    },
    {
      label: 'Plan Management',
      name: 'plan_mgnt',
      isChecked: false
    }
  ]

  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private roleService: RolesService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
  ) {
    this.roleID = this._activateRouter.snapshot.params['id'];
    this.pageNo = this._activateRouter.snapshot.params['page'];
    this.createForm();
  }

  ngOnInit(): void {


    if (this.roleID) {
      this.conditionalForm = true;
      this.spinner.show();
      this.roleService.get(this.roleID).subscribe((res: any) => {
        if (res.success) {
          this.roleForm.patchValue({
            name: res.data.name
          });
          if (res.data.permission.length > 0) {
            this.rolePermissions = res.data.permission;
          }
          console.log(this.rolePermissions, "this.rolePermissions")

        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
    } else {
      this.conditionalForm = false;
    }

  }


  createForm() {
    this.roleForm = this.formBuilder.group({
      name: ['', Validators.required],
      permission: []
    });
  }

  get f() { return this.roleForm.controls; }


  onSubmit() {
    this.submitted = true;
    this.roleForm.controls.permission.setValue(this.rolePermissions);

    if (!this.roleForm.invalid) {
      this.spinner.show();

      if (this.conditionalForm) {
        this._roleObservable = this.roleService.update(this.roleForm.value, this.roleID).subscribe(res => {
          let url;
          if (res.success) {
            this.toastr.success('Role updated successfully!');
            url = '/rolesAndPermissions/rolesAndPermissions/' + this.pageNo;
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          });

      } else {
        this._roleObservable = this.roleService.add(this.roleForm.value).subscribe(res => {
          let url;
          if (res.success) {
            this.toastr.success('Role added Successfully!');
            url = '/rolesAndPermissions';
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          });
      }
    }
  }

  ngOnDestroy(): void {
    if (this._roleObservable) {
      this._roleObservable.unsubscribe();
    }
  }




}
