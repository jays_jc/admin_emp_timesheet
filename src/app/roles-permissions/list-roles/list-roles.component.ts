import { Component, OnInit } from '@angular/core';
import { RolesService } from '../roles.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list-roles',
  templateUrl: './list-roles.component.html',
  styleUrls: ['./list-roles.component.scss']
})
export class ListRolesComponent implements OnInit {

  public roleData: Array<any> = [];
  public Permiss = [];
  public response: any;
  public response2: any;

  rows = [];
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 1;
  isDeleted = false
  filters: { page: number; search: string } = { page: 1, search: '' };

  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private roleService: RolesService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router,
    private _activateRouter: ActivatedRoute
  ) {
    if (this._activateRouter.snapshot.params['page']) {

      this.filters.page = JSON.parse(this._activateRouter.snapshot.params['page']);
      this.page = this.filters.page;
      console.log(this.filters.page, typeof (this._activateRouter.snapshot.params['page']))
      Object.assign(this.filters, { page: this.filters.page });
      this.getRoles();
    } else {
      this.page = 1
      this.getRoles();
    }
  }

  ngOnInit(): void {

  }


  edit(ID) {
    let route = '/rolesAndPermissions/edit/' + ID + "/" + this.page;
    this.router.navigate([route]);
  }

  /* Function use to remove Crop with crop id */
  remove(ID) {
    if (confirm("Do you want to delete this role ?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'roles'
      }

      this.roleService.deleteRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getRoles();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  /* Function use to remove Crop with crop id */
  moveBack(ID) {
    if (confirm("Do you want to move back this role to listing ?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'roles'
      }

      this.roleService.moveBackRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Role Moved Back Successfully');
        } else {
          this.toastr.error('Unable to move at the moment, Please try again later', 'Error');
        }
        this.getRoles();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  /* Function use to remove Crop with crop id */
  removePermanent(ID) {
    if (confirm("Do you want to delete this Role permanently?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'roles'
      }

      this.roleService.deleteRecordPermanent(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Role Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getRoles();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  getRoles() {
    this.spinner.show();
    // this.isLoading = true;
    this._subscriberData = this.roleService.getAllRoles(this.filters).subscribe((response) => {
      if (response.data.length == 0) {
        console.log("in if")
        this.roleData = [];
        // this.isLoading = false;
        this.spinner.hide();
      } else {
        console.log("in else")
        this.roleData = response.data.map(data => {
          return {
            id: data.id,
            name: data.name,
            permission: data.permission.filter(per => per.isChecked == true),
            deletedBy: data.deletedBy,
            deletedAt: data.deletedAt
          }
        });

        // this.isLoading = false;
        this.spinner.hide();
      }
    });
  }

  changeStatus(data, ID) {
    let Status = '';
    let message = 'Do you want to activate this role ?';
    if (data == 'active') {
      Status = "deactive";
      message = 'Do you want to deactivate this role ?';
    } else {
      Status = "active";
    }


    if (confirm(message)) {
      this.spinner.show();
      this.roleService.status(ID, 'roles', Status).subscribe((res: any) => {
        if (res.success) {
          this.response2 = res;
          this.toastr.success(res.data.message);
          this.getRoles();
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    let route = '/rolesAndPermissions/rolesAndPermissions/' + this.page;
    this.router.navigate([route]);
    this.getRoles();
  }

  searchValue() {
    this.page = 1;
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getRoles();
  }

  clearValue() {
    this.page = 1;
    this.filters.search = '';
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getRoles();
  }



}

