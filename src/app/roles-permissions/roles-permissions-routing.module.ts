import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddUpdateRolesComponent } from './add-update-roles/add-update-roles.component';
import { ListRolesComponent } from './list-roles/list-roles.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Roles'
    },
    children: [
     {
        path: '',
        component: ListRolesComponent
      }, {
        path: 'rolesAndPermissions/:page',
        component: ListRolesComponent
      },
      {
        path: 'list',
        component: ListRolesComponent
      },
      {
        path: 'add',
        component: AddUpdateRolesComponent
      },
      {
        path: 'edit/:id',
        component: AddUpdateRolesComponent
      },{
        path: 'edit/:id/:page',
        component: AddUpdateRolesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesPermissionsRoutingModule { }
