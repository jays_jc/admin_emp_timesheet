// import { NgModule } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';


// const routes: Routes = [];

// @NgModule({
//   imports: [RouterModule.forChild(routes)],
//   exports: [RouterModule]
// })
// export class NewsletterSubscribersRoutingModule { }
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ListComponent } from './list/list.component';
import { SendMailComponent } from './send-mail/send-mail.component';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Newsletter'
    },
    
        children: [
          {
              path: '',
              component: ListComponent
          },
          {
            path: 'sendmail',
            component: SendMailComponent
        }
        ]
      }
];

@NgModule({
  imports: [RouterModule.forChild(routes), FormsModule,
    HttpClientModule],
  exports: [RouterModule, FormsModule]
})
export class NewsletterSubscribersRoutingModule { }