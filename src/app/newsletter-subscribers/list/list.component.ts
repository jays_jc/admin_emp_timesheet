// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-list',
//   templateUrl: './list.component.html',
//   styleUrls: ['./list.component.css']
// })
// export class ListComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { NewsletterSubscribersService } from '../newsletter-subscribers.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {

  public data: any = [];
  public searchTerm = '';
  public activePage = '';
  public itemsTotal = 0;
  public rowsOnPage = 5;
  public sortBy = "createdAt";
  public sortOrder = "desc";
  public sortTrem = '';
  newsletter: any

  public response: any;
  public response2: any;

  public itemsOnPage;

  rows = [];
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  filters: { page: number; search: string, isDeleted: boolean } = { page: 1, search: '', isDeleted: false };

  constructor(
    private _router: Router,
    private _newsletterService: NewsletterSubscribersService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router
  ) { }

  ngOnInit() {
    this.getAllSubscribers();
  }


  changeStatus(data, ID) {
    let Status = '';
    let message = 'Do you want to activate this subscriber ?';
    if (data == 'active') {
      Status = "deactive";
      message = 'Do you want to deactivate this subscriber ?';
    } else {
      Status = "active";
    }


    if (confirm(message)) {
      this.spinner.show();
      this._newsletterService.changeStatus(ID, 'subscribers', Status).subscribe((res: any) => {
        if (res.success) {
          this.response2 = res;
          this.toastr.success(res.message);
          this.getAllSubscribers();
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error(err.message, 'Error');
      });
    }
  }

  getAllSubscribers(): void {
    this.spinner.show();
    this._newsletterService.allsubscribers(this.filters).subscribe((res: any) => {
      this.spinner.hide();
      console.log("res", res)
      if (res.success) {
        this.data = res.data;
        this.itemsTotal = res.total;
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.toastr.error('There are some errors, please try again after some time !', 'Error');
    });
  }

  save() {
    this.spinner.show();
    let data = JSON.parse(JSON.stringify(this.newsletter));
    this._newsletterService.add(data).subscribe((res: any) => {
      this.spinner.show();
      if (res.success) {
        this.response = res;
        this.newsletter = {
          subject: '',
          desc: '',
        };
        this.toastr.success(res.message);
        this._router.navigate(['/newsletter/list']);
      } else {
        this.toastr.error(res.error.message);
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
    });
  }

  trim(key) {
    if (this.newsletter[key] && this.newsletter[key][0] == ' ') this.newsletter[key] = this.newsletter[key].trim();
  }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    this.getAllSubscribers();
  }

  searchValue() {
    this.page = 1;
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getAllSubscribers();
  }

  clearValue() {
    this.page = 1;
    this.filters.search = '';
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getAllSubscribers();
  }

}
