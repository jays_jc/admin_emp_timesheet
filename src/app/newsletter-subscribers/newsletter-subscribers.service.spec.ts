import { TestBed } from '@angular/core/testing';

import { NewsletterSubscribersService } from './newsletter-subscribers.service';

describe('NewsletterSubscribersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewsletterSubscribersService = TestBed.get(NewsletterSubscribersService);
    expect(service).toBeTruthy();
  });
});
