// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';

// import { NewsletterSubscribersRoutingModule } from './newsletter-subscribers-routing.module';
// import { ListComponent } from './list/list.component';


// @NgModule({
//   declarations: [ListComponent],
//   imports: [
//     CommonModule,
//     NewsletterSubscribersRoutingModule
//   ]
// })
// export class NewsletterSubscribersModule { }
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
// import { NewsletterService } from './newsletter.service';
import { CustomFormsModule } from 'ng2-validation';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from '../shared/auth-interceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NewsletterSubscribersRoutingModule } from './newsletter-subscribers-routing.module';
import { NewsletterSubscribersService } from './newsletter-subscribers.service';
import { SendMailComponent } from './send-mail/send-mail.component';



@NgModule({
  declarations: [ListComponent, SendMailComponent],
  imports: [
    CommonModule,
    NewsletterSubscribersRoutingModule,
    CustomFormsModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxSpinnerModule
  ],
  providers: [
    NewsletterSubscribersService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
})
export class NewsletterSubscribersModule { }
