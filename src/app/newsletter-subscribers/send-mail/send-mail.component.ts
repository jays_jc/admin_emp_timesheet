// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-send-mail',
//   templateUrl: './send-mail.component.html',
//   styleUrls: ['./send-mail.component.scss']
// })
// export class SendMailComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
import { Component, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewsletterSubscribersService } from '../newsletter-subscribers.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-send-mail',
  templateUrl: './send-mail.component.html',
  styleUrls: ['./send-mail.component.scss']
})

export class SendMailComponent {
  public newsletter = {
    subject: '',
    description: '',
  };
  public emailForm: FormGroup;
  submitted = false;
  public roleID: any;
  public response: any;

  constructor(private _router: Router,
    private _activateRouter: ActivatedRoute,
    private _newsletterService: NewsletterSubscribersService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef) {
    this.createForm();
  }

  ngOnInit() {

  }
  createForm() {
    this.emailForm = this.formBuilder.group({
      subject: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  get f() { return this.emailForm.controls; }
  /*If id exist then will update existing dispensary otherwise will add new dispensary*/
  save() {
    this.submitted = true
    if (!this.emailForm.invalid) {
      this.spinner.show();
      let data = this.emailForm.value;
      this._newsletterService.add(data).subscribe((res: any) => {
        this.spinner.show();
        if (res.success) {
          this.response = res;
          this.submitted = false
          this.emailForm.reset()
          this.toastr.success(res.message);

          this._router.navigate(['/newsletter']);
        } else {
          this.toastr.error(res.error.message);
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
      });
    }

  }

  trim(key) {
    if (this.newsletter[key] && this.newsletter[key][0] == ' ') this.newsletter[key] = this.newsletter[key].trim();
  }
}

