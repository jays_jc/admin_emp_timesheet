// import { Injectable } from '@angular/core';

// @Injectable({
//   providedIn: 'root'
// })
// export class NewsletterSubscribersService {

//   constructor() { }
// }
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { CredentialsService } from '../auth/credentials.service';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsletterSubscribersService {

  private _baseUrl = environment.url;

  constructor(
    private httpClient: HttpClient,
    private credentialsService: CredentialsService,
    private spinner: NgxSpinnerService,) {
  }

  /*Use to fetch all Inputs*/
  allsubscribers(param?) {
    let params = new HttpParams();
    if (param) {
      for (let key of Object.keys(param)) {
        params = params.set(key, param[key])
      }
    }
    return this.httpClient.get(this._baseUrl + 'subscribers', { params: params }).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    )
  }

  /*Use to add new Input*/
  add(newsletter) {
    return this.httpClient.post(this._baseUrl + 'email/subscribers', newsletter).pipe(
      map((response: any) => {
        return response;
      }),
      catchError(this.handleError)
    )
  }

  /*Use to get category with id*/
  get(ID) {
    return this.httpClient.get(this._baseUrl + 'role/' + ID);
  }

  /*Use to update category*/
  update(cat) {
    return this.httpClient.put(this._baseUrl + 'role/' + cat.id, cat);
  }

  /*Use to soft delete any Record */
  deleteRecord(id, model) {

    // let headers = this.getAuthorizationHeader();
    let url = this._baseUrl + 'delete?id=' + id + '&model=' + model;
    return this.httpClient.delete(url);
  }

  /*Use to soft delete any Record */
  changeStatus(id, model, status) {
    let url = this._baseUrl + 'changestatus?id=' + id + '&model=' + model + '&status=' + status;
    return this.httpClient.put(url, {});
  }

  loader(key) {
    if (key == 'show') this.spinner.show();
    if (key == 'hide') this.spinner.hide();
  }

  getParams(parameters) {
    let params = new HttpParams();
    Object.keys(parameters).map((key) => {
      params = params.set(key, parameters[key]);
    })
    return params;
  }
  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error.code == 401) {
      return throwError('');
    } else if (error.error.code == 404) {
      return throwError(error.error.message);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}

