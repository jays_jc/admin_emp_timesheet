import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUpdateCategoryTypeComponent } from './add-update-category-type.component';

describe('AddUpdateCategoryTypeComponent', () => {
  let component: AddUpdateCategoryTypeComponent;
  let fixture: ComponentFixture<AddUpdateCategoryTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUpdateCategoryTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUpdateCategoryTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
