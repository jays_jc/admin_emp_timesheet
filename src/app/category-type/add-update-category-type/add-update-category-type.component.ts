import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CategoryTypeService } from '../category-type.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-update-category-type',
  templateUrl: './add-update-category-type.component.html',
  styleUrls: ['./add-update-category-type.component.scss']
})
export class AddUpdateCategoryTypeComponent implements OnInit {

  public categoryID: any;
  public response: any;
  conditionalForm: boolean = false;

  public catTypeForm: FormGroup;
  submitted = false;
  _typeObservable: any;

  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private typeService: CategoryTypeService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.categoryID = this._activateRouter.snapshot.params['id'];

    if (this.categoryID) {
      this.conditionalForm = true;
      this.spinner.show();
      this.typeService.getCatType(this.categoryID).subscribe((res: any) => {
        if (res.code == 200) {
          this.catTypeForm.patchValue({
            name: res.data.name
          });
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          this.toastr.error(error, 'Error');
        });
    } else {
      this.conditionalForm = false;
    }

  }

  createForm() {
    this.catTypeForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
  }

  get f() { return this.catTypeForm.controls; }


  onSubmit() {
    this.submitted = true;
    if (!this.catTypeForm.invalid) {
      this.spinner.show();

      if (this.conditionalForm) {
        this._typeObservable = this.typeService.update(this.catTypeForm.value, this.categoryID).subscribe(res => {
          let url;
          if (res.code == 200) {
            this.toastr.success('Category type updated Successfully!');
            url = '/categoryType';
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            this.toastr.error(error, 'Error');
          });

      } else {
        this._typeObservable = this.typeService.add(this.catTypeForm.value).subscribe(res => {
          let url;
          if (res.code == 200) {
            this.toastr.success('Category type added Successfully!');
            url = '/categoryType';
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            this.toastr.error(error, 'Error');
          });
      }
    }
  }

  ngOnDestroy(): void {
    if (this._typeObservable) {
      this._typeObservable.unsubscribe();
    }
  }




}
