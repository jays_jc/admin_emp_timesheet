import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCategoryTypeComponent } from './list-category-type/list-category-type.component';
import { AddUpdateCategoryTypeComponent } from './add-update-category-type/add-update-category-type.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Category-Type'
    },
    children: [
     {
        path: 'categoryType/:page',
        component: ListCategoryTypeComponent
      },{
        path: '',
        component: ListCategoryTypeComponent
      },
      {
        path: 'list',
        component: ListCategoryTypeComponent
      },
      {
        path: 'add',
        component: AddUpdateCategoryTypeComponent
      },
      {
        path: 'edit/:id',
        component: AddUpdateCategoryTypeComponent
      }, {
        path: 'edit/:id/:page',
        component: AddUpdateCategoryTypeComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryTypeRoutingModule { }
