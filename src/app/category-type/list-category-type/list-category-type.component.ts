import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { CategoryTypeService } from '../category-type.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-category-type',
  templateUrl: './list-category-type.component.html',
  styleUrls: ['./list-category-type.component.scss']
})
export class ListCategoryTypeComponent implements OnInit {
  actionType = 'active'
  public catData: Array<any> = [];
  public response: any;
  public response2: any;

  rows = [];
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  isDeleted = false
  filters: { page: number; search: string, isDeleted: boolean } = { page: 1, search: '', isDeleted: false };

  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private catTypeService: CategoryTypeService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router,
    private _activateRouter: ActivatedRoute
  ) {
    if (this._activateRouter.snapshot.params['page']) {

      this.filters.page = JSON.parse(this._activateRouter.snapshot.params['page']);
      this.page = this.filters.page;
      console.log(this.filters.page, typeof (this._activateRouter.snapshot.params['page']))
      Object.assign(this.filters, { page: this.filters.page });
      this.getCategory();
    } else {
      this.page = 1
      this.getCategory();
    }

    this.catTypeService.actionType.subscribe(res => {
      console.log('resdsf', res)
      if (res !== '') {
        this.actionType = res
        if (this.actionType == 'active') {
          this.filters.isDeleted = false;

        } else if (this.actionType == 'deleted') {
          this.filters.isDeleted = true;
        }
      } else {
        this.actionType = 'active'
      }

    })
    console.log('this.actionType', this.actionType)
  }

  ngOnInit(): void {

  }
  add() {
    let route = '/categoryType/add/';
    this.catTypeService.actionType.next('active')
    this.router.navigate([route]);
  }
  edit(ID, type) {
    this.catTypeService.actionType.next(type)
    this.catTypeService.activePage.next(this.page)
    let route = '/categoryType/edit/' + ID + '/' + this.page;
    this.router.navigate([route]);
  }

  /* Function use to remove Crop with crop id */
  remove(ID) {
    if (confirm("Do you want to delete this category type?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'categorytype'
      }

      this.catTypeService.deleteRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Category Type Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getCategory();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  /* Function use to remove Crop with crop id */
  moveBack(ID) {
    if (confirm("Do you want to move back this category type to listing ?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'categorytype'
      }

      this.catTypeService.moveBackRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Category type Moved Back Successfully');
        } else {
          this.toastr.error('Unable to move at the moment, Please try again later', 'Error');
        }
        this.getCategory();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  getCategory() {
    this.spinner.show();
    this.isLoading = true;
    this._subscriberData = this.catTypeService.getAllCategory(this.filters).subscribe((response) => {

      if (response['data'].length == 0) {
        this.catData = [];
        this.isLoading = false;
        this.spinner.hide();
      } else {
        this.catData = response['data'].map(cat => {
          return {
            id: cat.id,
            catName: cat.name,
            date: cat.createdAt,
            status: cat.status,
            deletedBy: cat.deletedBy,
            deletedAt: cat.deletedAt
          }
        });
        this.isLoading = false;
        this.spinner.hide();
      }
    });
  }

  /* Function use to remove Crop with crop id */
  removePermanent(ID) {
    if (confirm("Do you want to delete this category type permanently?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'categorytype'
      }

      this.catTypeService.deleteRecordPermanent(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Category Type Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getCategory();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error(err.message, 'Error');
      });
    }
  }

  changeStatus(data, ID) {
    let Status = '';
    let message = 'Do you want to activate this category type ?';
    if (data == 'active') {
      Status = "deactive";
      message = 'Do you want to deactivate this category type ?';
    } else {
      Status = "active";
    }


    if (confirm(message)) {
      this.spinner.show();
      this.catTypeService.status(ID, 'categorytype', Status).subscribe((res: any) => {
        if (res.success) {
          this.response2 = res;
          this.toastr.success(res.message);
          this.getCategory();
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    let route = '/categoryType/categoryType/' + this.page;
    this.router.navigate([route]);
    this.getCategory();
  }

  searchValue() {
    this.page = 1;
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getCategory();
  }

  clearValue() {
    this.page = 1;
    this.filters.search = '';
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getCategory();
  }



}