import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCategoryTypeComponent } from './list-category-type.component';

describe('ListCategoryTypeComponent', () => {
  let component: ListCategoryTypeComponent;
  let fixture: ComponentFixture<ListCategoryTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCategoryTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCategoryTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
