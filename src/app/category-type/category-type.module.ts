import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';

import { CategoryTypeRoutingModule } from './category-type-routing.module';
import { ListCategoryTypeComponent } from './list-category-type/list-category-type.component';
import { AddUpdateCategoryTypeComponent } from './add-update-category-type/add-update-category-type.component';

import { CategoryTypeService } from './category-type.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


@NgModule({
  declarations: [ListCategoryTypeComponent, AddUpdateCategoryTypeComponent],
  imports: [
    CommonModule,
    CategoryTypeRoutingModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule
  ],
  providers: [CategoryTypeService]
})
export class CategoryTypeModule { }
