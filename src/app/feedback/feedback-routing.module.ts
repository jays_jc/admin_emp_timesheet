import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeedbackeditComponent } from './feedbackedit/feedbackedit.component';
import { FeedbacklistComponent } from './feedbacklist/feedbacklist.component';
import { FeedbackreplyComponent } from './feedbackreply/feedbackreply.component';
import { FeedbackviewComponent } from './feedbackview/feedbackview.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Feedback'
    },
    children: [
     {
        path: '',
        component: FeedbacklistComponent
      },{
        path: 'feedback/:page',
        component: FeedbacklistComponent
      }, {
        path: 'edit/:id/:page',
        component: FeedbackeditComponent
      },
      {
        path: 'reply/:id/:page',
        component: FeedbackreplyComponent
      },
      {
        path: 'view/:id/:page',
        component: FeedbackviewComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedbackRoutingModule { }
