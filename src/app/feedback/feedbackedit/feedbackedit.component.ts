import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { FeedbackService } from '../feedback.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-feedbackedit',
  templateUrl: './feedbackedit.component.html',
  styleUrls: ['./feedbackedit.component.scss']
})
export class FeedbackeditComponent implements OnInit {
  pageNo:any;
  public _host = environment.url;
  submitted:boolean = false;
  public feedbackID: any;
  public feedback: any = {};
  conditionalForm: boolean = false;
  public popupImage = '';
editForm:FormGroup;
  constructor(
    private router : Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private feedbackService: FeedbackService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal ) { }
  ngOnInit() {

    this.editForm = this.formBuilder.group({
      message:['',[Validators.required]],
      email:['',[Validators.required]],
      title:['',[Validators.required]]
    })
    this.pageNo = this._activateRouter.snapshot.params['page'];

    this.feedbackID = this._activateRouter.snapshot.params['id']; 
    if(this.feedbackID){
      this.conditionalForm = true;
      this.spinner.show();
      this.feedbackService.get(this.feedbackID).subscribe((res: any) => {
        if (res.success) {
          this.feedback = res.data;
          this.editForm.patchValue(this.feedback)  
          
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        // this.toastr.error(error, 'Error');
      });
    }else{
     this.conditionalForm = false;
    }
  }
  get f() { return this.editForm.controls; }
  onSubmit(){
    this.submitted = true;
    if(!this.editForm.invalid){
      this.spinner.show();
      let data = {
        email:this.editForm.controls.email.value,
        message:this.editForm.controls.message.value,
        title:this.editForm.controls.title.value, 
        id:this.feedbackID
      } 
      this.feedbackService.update(data).subscribe((res)=>{
      if(res.success){
        this.toastr.success('Feedback has been updated')
        this.router.navigateByUrl('/feedback/feedback/'+this.pageNo)
      }else{
        this.toastr.error(res.error.message, 'Error');
      }
      this.spinner.hide();
      })
    }
  }


}
