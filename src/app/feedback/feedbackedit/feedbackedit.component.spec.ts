import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedbackeditComponent } from './feedbackedit.component';

describe('FeedbackeditComponent', () => {
  let component: FeedbackeditComponent;
  let fixture: ComponentFixture<FeedbackeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbackeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
