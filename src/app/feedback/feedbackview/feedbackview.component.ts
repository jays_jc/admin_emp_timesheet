import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { FeedbackService } from '../feedback.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-feedbackview',
  templateUrl: './feedbackview.component.html',
  styleUrls: ['./feedbackview.component.scss']
})
export class FeedbackviewComponent implements OnInit {

  pageNo:any;
  public _host = environment.url;

  public feedbackID: any;
  public feedback: any = {};
  conditionalForm: boolean = false;
  public popupImage = '';

  constructor(
    private router : Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private feedbackService: FeedbackService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal ) { }

  ngOnInit(): void {
  this.pageNo = this._activateRouter.snapshot.params['page'];

    this.feedbackID = this._activateRouter.snapshot.params['id']; 
    
    if(this.feedbackID){
      this.conditionalForm = true;
      this.spinner.show();
      this.feedbackService.get(this.feedbackID).subscribe((res: any) => {
        if (res.success) {
          this.feedback = res.data;
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        // this.toastr.error(error, 'Error');
      });
    }else{
     this.conditionalForm = false;
    }
  }

  openModal(img) {
    this.popupImage = img;
  }



}
