import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ToastrService } from 'ngx-toastr';
import {FeedbackService} from '../feedback.service'

@Component({
  selector: 'app-feedbacklist',
  templateUrl: './feedbacklist.component.html',
  styleUrls: ['./feedbacklist.component.scss']
})
export class FeedbacklistComponent implements OnInit {
  actionType = 'active'
  public Data: Array<any> = []; 
  public response: any;
  public response2: any;

  rows = [];
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 1;
  isDeleted = false
  filters: { page: number; search: string, isDeleted: boolean } = { page: 1, search: '', isDeleted: false };

  constructor(private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router,
    private feedbackService:FeedbackService,
    private _activateRouter:ActivatedRoute) { 
      this.filters.page = 1 
      if(this._activateRouter.snapshot.params['page']){
     
        this.filters.page = JSON.parse(this._activateRouter.snapshot.params['page'])  ;
        this.page  = this.filters.page;
        console.log(this.filters.page,typeof(this._activateRouter.snapshot.params['page']))
        Object.assign(this.filters, { page: this.filters.page });  
      
      }else{ 
        this.page = 1 
      }
    
   
      
     }

  ngOnInit() { 
    this.getFeedback();
    // comment code 
    // coment code
   
  }


  add() {
    let route = '/feedback/add/';
    // this.feedbackService.actionType.next('active')
    this.router.navigate([route]);
  }
  edit(ID,type) {
    // this.feedbackService.actionType.next(type)
    // this.feedbackService.activePage.next(this.page)
    // let route = '/feedback/edit/' + ID+'/'+this.page;
    let route = '/feedback/edit/'+ID+"/"+this.page;
    this.router.navigate([route]);
  }

  view(ID) {
    // this.feedbackService.actionType.next(type)
    // this.feedbackService.activePage.next(this.page)
    // let route = '/feedback/edit/' + ID+'/'+this.page;
    let route = '/feedback/view/'+ID+"/"+this.page;
    this.router.navigate([route]);
  }

  reply(ID,type) {
    // this.feedbackService.actionType.next(type)
    // this.feedbackService.activePage.next(this.page)
    // let route = '/categoryType/edit/' + ID+'/'+this.page;
    let route = '/feedback/reply/'+ID+"/"+this.page
    this.router.navigate([route]);
  }

  /* Function use to remove Crop with crop id */
  remove(ID) {
    if (confirm("Do you want to delete this feedback type?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'feedback'
      }

      // this.feedbackService.deleteRecord(obj).subscribe((res: any) => {
      //   if (res.success) {
      //     this.response = res;
      //     this.toastr.success('Category Type Deleted Successfully');
      //   } else {
      //     this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
      //   }
      //   this.getFeedback();
      //   this.spinner.hide();
      // }, err => {
      //   this.spinner.hide();
      //   this.toastr.error('There are some errors, please try again after some time !', 'Error');
      // });
    }
  }

  /* Function use to remove Crop with crop id */
  moveBack(ID) {
    if (confirm("Do you want to move back this category type to listing ?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'categorytype'
      }

      // this.feedbackService.moveBackRecord(obj).subscribe((res: any) => {
      //   if (res.success) {
      //     this.response = res;
      //     this.toastr.success('Category type Moved Back Successfully');
      //   } else {
      //     this.toastr.error('Unable to move at the moment, Please try again later', 'Error');
      //   }
      //   this.getFeedback();
      //   this.spinner.hide();
      // }, err => {
      //   this.spinner.hide();
      //   this.toastr.error('There are some errors, please try again after some time !', 'Error');
      // });
    }
  }

  getFeedback() {
    this.spinner.show();
    this.isLoading = true;
    this._subscriberData = this.feedbackService.getAllFeedback(this.filters).subscribe((response) => {
console.log('response',response)
      if (response['data'].length == 0) {
        this.Data = [];
        this.isLoading = false;
        this.spinner.hide();
      } else {
        this.Data = response['data'].map(feedback => {
          return {
            id: feedback.id,
            title: feedback.title,
            email: feedback.email,
            message: feedback.message,
            
          }
        });
        this.isLoading = false;
        this.spinner.hide();
      }
    });
  }

  /* Function use to remove Crop with crop id */
  removePermanent(ID) {
    if (confirm("Do you want to delete this feedback   permanently?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'feedback'
      }

      // this.feedbackService.deleteRecordPermanent(obj).subscribe((res: any) => {
      //   if (res.success) {
      //     this.response = res;
      //     this.toastr.success('Category Type Deleted Successfully');
      //   } else {
      //     this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
      //   }
      //   this.getFeedback();
      //   this.spinner.hide();
      // }, err => {
      //   this.spinner.hide();
      //   this.toastr.error(err.message, 'Error');
      // });
    }
  }

  changeStatus(data, ID) {
    let Status = '';
    let message = 'Do you want to activate this feedback type ?';
    if (data == 'active') {
      Status = "deactive";
      message = 'Do you want to deactivate this feedback type ?';
    } else {
      Status = "active";
    }


    if (confirm(message)) {
      // this.spinner.show();
      // this.feedbackService.status(ID, 'categorytype', Status).subscribe((res: any) => {
      //   if (res.success) {
      //     this.response2 = res;
      //     this.toastr.success(res.message);
      //     this.getFeedback();
      //   } else {
      //     this.toastr.error(res.error.message, 'Error');
      //   }
      //   this.spinner.hide();
      // }, err => {
      //   this.spinner.hide();
      //   this.toastr.error('There are some errors, please try again after some time !', 'Error');
      // });
    }
  }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    let route = '/feedback/feedback/' + this.page;
    this.router.navigate([route]);
    this.getFeedback();
  }

  searchValue() {
    this.page = 1;
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getFeedback();
  }

  clearValue() {
    this.page = 1;
    this.filters.search = '';
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getFeedback();
  }

}
