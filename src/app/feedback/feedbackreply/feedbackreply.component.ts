import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { FeedbackService } from '../feedback.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-feedbackreply',
  templateUrl: './feedbackreply.component.html',
  styleUrls: ['./feedbackreply.component.scss']
})
export class FeedbackreplyComponent implements OnInit {
  replyForm: FormGroup;
  pageNo: any;
  submitted: boolean = false;
  feedbackID: any;
  feedback: any;
  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private feedbackService: FeedbackService,
    private formBuilder: FormBuilder) {
    this.pageNo = this._activateRouter.snapshot.params['page'];
    this.feedbackID = this._activateRouter.snapshot.params['id'];
  }

  ngOnInit() {
    this.getFeedbackData();
    this.replyForm = this.formBuilder.group({
      message: ['', [Validators.required]],
      email: ['', [Validators.required]]
    })

  }
  getFeedbackData() {
    if (this.feedbackID) {
      this.spinner.show();
      this.feedbackService.get(this.feedbackID).subscribe((res: any) => {
        if (res.success) {
          this.feedback = res.data;
          this.replyForm.controls.email.setValue(this.feedback.email)
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
    }
  }
  get f() { return this.replyForm.controls; }
  onSubmit() {
    this.submitted = true;
    console.log('submit', this.replyForm.value)
    if (this.replyForm.invalid) {
      return
    }
    if (!this.replyForm.invalid) {
      this.spinner.show();
      let data = {
        email: this.replyForm.controls.email.value,
        message: this.replyForm.controls.message.value,
        feedback_id: this.feedbackID,
        is_reply: "yes"

      }
      this.feedbackService.addReply(data).subscribe((res) => {
        if (res.success) {
          this.toastr.success('Reply has been sent')
          this.router.navigateByUrl('/feedback/feedback/' + this.pageNo)
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      })
    }
  }




}
