import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedbacklistComponent } from './feedbacklist/feedbacklist.component';
import { FeedbackreplyComponent } from './feedbackreply/feedbackreply.component';
import { FeedbackeditComponent } from './feedbackedit/feedbackedit.component';
import { FeedbackRoutingModule } from './feedback-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FeedbackviewComponent } from './feedbackview/feedbackview.component';


@NgModule({
  declarations: [FeedbacklistComponent, FeedbackreplyComponent, FeedbackeditComponent, FeedbackviewComponent],
  imports: [
    CommonModule,
    FeedbackRoutingModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule
  ]
})
export class FeedbackModule { }
