import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from '../auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {

  public adminLoginForm: FormGroup;
  submitted = false;
  public rememberMe: boolean = false;
  _loginObservable: any;
  passwordType: boolean;

  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    if (localStorage.getItem("remember")) {
      this.adminLoginForm.controls.email.setValue(localStorage.getItem("remember"));
    }
  }


  markRemember() {
    this.rememberMe = !this.rememberMe
  }

  createForm() {
    this.adminLoginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      remember: [''],
      // role: ['SA']
    });
  }

  get f() { return this.adminLoginForm.controls; }

  togglePassword() {
    this.passwordType = !this.passwordType;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.adminLoginForm.invalid) {
      this.spinner.show();
      this._loginObservable = this.authService.login(this.adminLoginForm.value).subscribe(res => {
        if (res.code == 200) {
          this.router.navigate(['/dashboard']);
          this.toastr.success('Logged In Successfully!');
        } else {
          this.toastr.error(res.error.message, 'Error');
        }

        if (this.rememberMe == true) {
          localStorage.setItem("remember", this.adminLoginForm.value.email);
        } else {
          localStorage.removeItem('remember');
        }

        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        }
      );
    }
  }

  ngOnDestroy(): void {
    if (this._loginObservable) {
      this._loginObservable.unsubscribe();
    }
  }

  openForgot() {
    const modalRef = this.modalService.open(ForgotPasswordComponent,
      { ariaLabelledBy: 'modal-basic-title', size: 'lg' });
  }



}
