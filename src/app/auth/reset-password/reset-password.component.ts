import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfirmMatch } from 'src/app/shared/confirm-match.validator';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  public resetPassForm: FormGroup;
  submitted = false;
  newPassTextType: boolean;
  cnfmPassTextType: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private authService: AuthService,
    private router: Router
  ) { this.createForm(); }

  createForm() {
    this.resetPassForm = this.formBuilder.group({
      code: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.minLength(5)]],
      confirmPassword: ['', Validators.required]
    },
      {
        validator: ConfirmMatch('newPassword', 'confirmPassword')
      }
    );
  }

  get f() { return this.resetPassForm.controls; }

  ngOnInit(): void {
  }

  toggleNewPassword() {
    this.newPassTextType = !this.newPassTextType;
  }

  toggleCnfmPassword() {
    this.cnfmPassTextType = !this.cnfmPassTextType;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.resetPassForm.invalid) {

      this.spinner.show();
      this.authService.resetPassword(this.resetPassForm.value).subscribe(res => {
        if (res.success) {
          this.toastr.success(res.message);
          this.resetPassForm.reset();
          this.submitted = false;
          this.router.navigate(['/']);
        } else {
          this.toastr.error(res.message);
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error);
        }
      )
    }
  }
}
