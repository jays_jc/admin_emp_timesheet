import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { UsersRoutingModule } from './users-routing.module';
import { UsersService } from './users.service';
import { ListUsersComponent } from './list-users/list-users.component';
import { ViewUsersComponent } from './view-users/view-users.component';
import { AddUpdateUsersComponent } from './add-update-users/add-update-users.component';


@NgModule({
  declarations: [ListUsersComponent, ViewUsersComponent, AddUpdateUsersComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    NgxSpinnerModule,
    NgxDatatableModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [UsersService]
})
export class UsersModule { }
