  import { Component, OnInit } from '@angular/core';
  import { Router, ActivatedRoute } from '@angular/router';
  import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
  import { FormGroup, FormBuilder, Validators } from '@angular/forms';
  import { NgxSpinnerService } from 'ngx-spinner';
  import { UsersService } from '../users.service';
  import { ToastrService } from 'ngx-toastr';
  
  @Component({
    selector: 'app-view-users',
    templateUrl: './view-users.component.html',
    styleUrls: ['./view-users.component.scss']
  })
  export class ViewUsersComponent implements OnInit {
  activePage:any;
    public userID: any;
    public user: any = {};
    conditionalForm: boolean = false;
  
    constructor(
      private router : Router,
      private _activateRouter: ActivatedRoute,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService,
      private userService: UsersService,
      private formBuilder: FormBuilder,
      private modalService: NgbModal ) { }
  
    ngOnInit(): void {
      this.userID = this._activateRouter.snapshot.params['id']; 
      this.activePage = this._activateRouter.snapshot.params['page']; 
      
      if(this.userID){
        this.conditionalForm = true;
        this.spinner.show();
        this.userService.getUser(this.userID).subscribe((res: any) => {
          if (res.success) {
            this.user = res.data;
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
      }else{
       this.conditionalForm = false;
      }
    }
  
  
  
  }
  
