import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUsersComponent } from './list-users/list-users.component';
import { ViewUsersComponent } from './view-users/view-users.component';
import { AddUpdateUsersComponent } from './add-update-users/add-update-users.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Users'
    },
    children: [
     {
        path: '',
        component: ListUsersComponent
      },
      {
        path: 'users/:page',
        component: ListUsersComponent
      },
      {
        path: 'list',
        component: ListUsersComponent
      },
      {
        path: 'add',
        component: AddUpdateUsersComponent
      },
      {
        path: 'edit/:id/:page',
        component: AddUpdateUsersComponent
      },
      {
        path: 'list/:id',
        component: ViewUsersComponent
      }, {
        path: 'list/:id/:page',
        component: ViewUsersComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
