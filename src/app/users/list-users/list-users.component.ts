import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {
  actionType: any = 'active'
  public userData: Array<any> = [];
  public response: any;
  public response2: any;

  rows = [];
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  role: any = '';
  isLoading: boolean = false;
  page = 1;
  isDeleted = false
  filters: { page: number; search: string, isDeleted: boolean } = { page: this.page, search: '', isDeleted: false };

  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private userService: UsersService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router,
    private _activateRouter: ActivatedRoute,
  ) {


    this.userService.actionType.subscribe(res => {
      if (res !== '' && res !== undefined && res !== null) {
        this.actionType = res
        if (this.actionType == 'active') {
          this.filters.isDeleted = false;

        } else if (this.actionType == 'deleted') {
          this.filters.isDeleted = true;
        }
      } else {
        this.actionType = 'active'
      }
    })
  }

  ngOnInit(): void {
    if (this._activateRouter.snapshot.params['page']) {

      this.filters.page = JSON.parse(this._activateRouter.snapshot.params['page']);
      this.page = this.filters.page;
      console.log(this.filters.page, typeof (this._activateRouter.snapshot.params['page']))
      Object.assign(this.filters, { page: this.filters.page });
      this.getUsers();
    } else {
      this.page = 1
      this.getUsers();
    }

  }
  filterData(value) {
    this.role = "";
    if (value === "" || value === "all") {
      delete this.filters['role'];
      this.getUsers();
    } else {
      this.role = value
      Object.assign(this.filters, { role: value });
      // this.role = value;  
      this.getUsers();
    }
    console.log('this.role', this.role)
  }
  viewUser(userID, type) {
    this.userService.actionType.next(type)
    let route = '/users/list/' + userID + '/' + this.page;
    this.router.navigate([route]);
  }
  add() {
    let route = '/users/add/';
    this.userService.actionType.next('active')
    this.router.navigate([route]);
  }
  edit(ID, type) {
    this.userService.actionType.next(type)
    this.userService.activePage.next(this.page)
    let route = '/users/edit/' + ID + '/' + this.page;
    this.router.navigate([route]);
  }

  /* Function use to remove Crop with crop id */
  remove(ID) {
    if (confirm("Do you want to delete this user?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'users'
      }

      this.userService.deleteRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('User Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getUsers();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  /* Function use to remove Crop with crop id */
  moveBack(ID) {
    if (confirm("Do you want to move back this user to listing ?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'users'
      }

      this.userService.moveBackRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('User Moved Back Successfully');
        } else {
          this.toastr.error('Unable to move at the moment, Please try again later', 'Error');
        }
        this.getUsers();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  getUsers() {
    this.spinner.show();
    this.isLoading = true;
    if (this.role) {
      Object.assign(this.filters, { role: this.role });
    }
    this._subscriberData = this.userService.getAllUsers(this.filters).subscribe((response) => {
      if (response.data.length == 0) {
        this.userData = [];
        this.isLoading = false;
        this.spinner.hide();
      } else {
        this.userData = response.data.map(data => {
          return {
            id: data.id,
            name: data.fullName,
            email: data.email,
            date: data.createdAt,
            status: data.status,
            deletedBy: data.deletedBy,
            deletedAt: data.deletedAt
          }
        });
        this.isLoading = false;
        this.spinner.hide();
      }
    });
  }

  /* Function use to remove Crop with crop id */
  removePermanent(ID) {
    if (confirm("Do you want to delete this user permanently?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'users'
      }

      this.userService.deleteRecordPermanent(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('User Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getUsers();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  changeStatus(data, ID) {
    let Status = '';
    let message = 'Do you want to activate this user ?';
    if (data == 'active') {
      Status = "deactive";
      message = 'Do you want to deactivate this user ?';
    } else {
      Status = "active";
    }


    if (confirm(message)) {
      this.spinner.show();
      this.userService.status(ID, 'users', Status).subscribe((res: any) => {
        if (res.success) {
          this.response2 = res;
          this.toastr.success(res.message);
          this.getUsers();
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    let route = '/users/users/' + this.page;
    this.router.navigate([route]);
    this.getUsers();
  }

  searchValue() {
    this.page = 1;
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getUsers();
  }

  clearValue() {
    this.page = 1;
    this.filters.search = '';
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getUsers();
  }



}
