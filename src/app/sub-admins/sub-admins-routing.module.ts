import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddUpdateComponent } from './add-update/add-update.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'subadmins'
    },
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'subadmins/:page',
        component: ListComponent
      },
      {
        path: 'list',
        component: ListComponent
      },
      {
        path: 'add',
        component: AddUpdateComponent
      },
      {
        path: 'edit/:id/:page',
        component: AddUpdateComponent
      },
      {
        path: 'list/:id',
        component: ViewComponent
      }, {
        path: 'list/:id/:page',
        component: ViewComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubAdminsRoutingModule { }
