// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';




// @NgModule({
//   declarations: [AddUpdateComponent, ListComponent, ViewComponent],
//   imports: [
//     CommonModule,
//     SubAdminsRoutingModule
//   ]
// })
// export class SubAdminsModule { }
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SubAdminsRoutingModule } from './sub-admins-routing.module';
import { AddUpdateComponent } from './add-update/add-update.component';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { SubAdminsService } from './sub-admins.service';


@NgModule({
  declarations: [AddUpdateComponent, ListComponent, ViewComponent],
  imports: [
    CommonModule,
    SubAdminsRoutingModule,
    NgxSpinnerModule,
    NgxDatatableModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SubAdminsService]
})
export class SubAdminsModule { }
