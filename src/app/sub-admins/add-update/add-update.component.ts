import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { SubAdminsService } from '../sub-admins.service';

@Component({
  selector: 'app-add-update',
  templateUrl: './add-update.component.html',
  styleUrls: ['./add-update.component.css']
})
export class AddUpdateComponent implements OnInit {
  pageNo: any;
  public userID: any;
  public response: any;
  conditionalForm: boolean = false;

  public userForm: FormGroup;
  submitted = false;
  _userObservable: any;

  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private subadminService: SubAdminsService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    // this.getRoles();

    this.userID = this._activateRouter.snapshot.params['id'];
    this.pageNo = this._activateRouter.snapshot.params['page'];

    if (this.userID) {
      this.conditionalForm = true;
      this.spinner.show();
      this.subadminService.getSubAdmin(this.userID).subscribe((res: any) => {
        if (res.success) {
          this.userForm.patchValue({
            firstName: res.data.firstName,
            lastName: res.data.lastName,
            email: res.data.email,
            role: res.data.role,
            mobileNo: res.data.mobileNo
          });
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
    } else {
      this.conditionalForm = false;
    }

  }

  createForm() {
    this.userForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mobileNo: ['', [Validators.required, Validators.min(1000000000), Validators.max(999999999999), Validators.pattern]]
    });
  }

  get f() { return this.userForm.controls; }


  // getRoles() {
  //   this.userService.getAllRoles().subscribe((response) => {
  //     if (response.data.length == 0) {
  //       this.roleData = [];
  //     } else {
  //       this.roles = response.data;
  //       this.roles.forEach(element => {
  //         if (element.name == "employer" || element.name == "sub_admin") {
  //           this.roleData.push(element)
  //         }
  //       });
  //     }
  //   });
  // }

  onSubmit() {
    this.submitted = true;
    if (!this.validateEmail(this.userForm.value.email)) {
      this.toastr.warning('Please enter a valid email.');
      return;
    }
    if (!this.userForm.invalid) {
      this.spinner.show();
      let data = this.userForm.value;
      data['id'] = this.userID

      if (this.conditionalForm) {
        this._userObservable = this.subadminService.update(data).subscribe(res => {
          let url;
          if (res.success) {
            this.toastr.success('Sub Admin Updated Successfully!');
            url = '/subadmins/subadmins/' + this.pageNo;
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          });

      } else {
        this._userObservable = this.subadminService.add(this.userForm.value).subscribe(res => {
          let url;
          if (res.success) {
            this.toastr.success('Sub Admin Added Successfully!');
            url = '/subadmins';
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          }
        );
      }
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  ngOnDestroy(): void {
    if (this._userObservable) {
      this._userObservable.unsubscribe();
    }
  }




}


