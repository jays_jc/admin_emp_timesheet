import { Component, OnInit, HostListener } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { CredentialsService } from 'src/app/auth/credentials.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  // public isLoggedIn: boolean = false;
  user: any;
  constructor(private credentialsService: CredentialsService) { }

  ngOnInit(): void {
    this.user = this.credentialsService.credentials ? this.credentialsService.credentials : '';
  }
}
