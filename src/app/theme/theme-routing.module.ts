import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AuthGuard } from '../shared/auth.guard';
import { BannerSettingsModule } from '../banner-settings/banner-settings.module';
// import { VacanciesModule } from '../vacancies/vacancies.module';
import { FeedbackModule } from '../feedback/feedback.module';
import { NewsletterSubscribersModule } from '../newsletter-subscribers/newsletter-subscribers.module';
import { CompaniesModule } from '../companies/companies.module';
// import { CommentsModule } from '../comments/comments.module';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'auth',
		pathMatch: 'full',
	},
	{
		path: 'auth',
		loadChildren: '../auth/auth.module#AuthModule'
		// loadChildren: () => AuthModule,
		// component: LayoutComponent,
		// canActivate: [ActiveRouteGuard]
	},
	{

		path: '',
		component: LayoutComponent,
		canActivate: [AuthGuard],
		children: [

			{
				path: 'dashboard',
				loadChildren: '../dashboard/dashboard.module#DashboardModule'
				// loadChildren: () => DashboardModule,
			},
			{
				path: 'users',
				loadChildren: '../users/users.module#UsersModule'
				// loadChildren: () => UsersModule,
			},
			{
				path: 'subadmins',
				loadChildren: '../sub-admins/sub-admins.module#SubAdminsModule'
				// loadChildren: () => UsersModule,
			},
			{
				path: 'categoryType',
				loadChildren: '../category-type/category-type.module#CategoryTypeModule'
				// loadChildren: () => CategoryTypeModule,
			},
			{
				path: 'category',
				loadChildren: '../category/category.module#CategoryModule'
			},
			{
				path: 'rolesAndPermissions',
				loadChildren: '../roles-permissions/roles-permissions.module#RolesPermissionsModule'
				// loadChildren: () => RolesPermissionsModule,
			},

			{
				path: 'blogs',
				loadChildren: '../blogs/blogs.module#BlogsModule'
				// loadChildren: () => BlogsModule,
			},
			{
				path: 'payments',
				loadChildren: '../payments/payments.module#PaymentsModule'
				// loadChildren: () => PaymentsModule,
			},
			{
				path: 'content',
				loadChildren: '../content/content.module#ContentModule'
				// loadChildren: () => ContentModule,
			},
			{
				path: 'faq',
				loadChildren: '../faq/faq.module#FaqModule'
				// loadChildren: () => FaqModule,
			},
			{
				path: 'settings',
				loadChildren: '../settings/settings.module#SettingsModule'
				// loadChildren: () => SettingsModule,
			},
			{
				path: 'banner',
				// loadChildren: '../settings/settings.module#SettingsModule'
				loadChildren: () => BannerSettingsModule
			},
			{
				path: 'profile',
				loadChildren: '../profile/profile.module#ProfileModule'
				// loadChildren: () => ProfileModule,
			},
			// {
			// 	path: 'vacancies',
			// 	// loadChildren: '../vacancies/vacancies.module#VacanciesModule'
			// 	loadChildren: () => VacanciesModule,
			// },
			{
				path: 'feedback',
				// loadChildren: '../vacancies/vacancies.module#VacanciesModule'
				loadChildren: () => FeedbackModule,
			},
			{
				path: 'companies',
				loadChildren: '../companies/companies.module#CompaniesModule'
				// loadChildren: () => CompaniesModule,
			},
			// {
			// 	path: 'comments',
			// 	// loadChildren: '../vacancies/vacancies.module#VacanciesModule'
			// 	loadChildren: () => CommentsModule,
			// },
			{
				path: 'newsletter',
				loadChildren: '../newsletter-subscribers/newsletter-subscribers.module#NewsletterSubscribersModule'
				// loadChildren: () => NewsletterSubscribersModule,
			}
		]
	},

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ThemeRoutingModule { }
