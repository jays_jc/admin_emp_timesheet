import { Component, OnInit, Input, ViewChild, ElementRef, NgZone } from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { CredentialsService } from 'src/app/auth/credentials.service';
import { BehaviorService } from "../../shared/behavior.service";
import { environment } from '../../../environments/environment';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {

  public _host = environment.url;
  updateData: any;
  public userID: any;
  public user: any = {};

  constructor(
    private credentialsService: CredentialsService,
    private router: Router,
    public _bs: BehaviorService
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    if (this.user) {
      this.userID = this.user.id;
    }

    this._bs.getUserData().subscribe((res: any) => {
      this.updateData = res;
      if (res.firstName)
        localStorage.setItem("user", JSON.stringify(this.updateData));
    });
  }

  logout() {
    this.credentialsService.logout().subscribe(res => {
      this.router.navigate(['/']);
    });
  }


}
