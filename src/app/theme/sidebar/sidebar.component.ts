import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public user = true;
  public catType = true;
  public category = true;
  public role = true;
  public product = true;
  public companies = true;
  public discount = true;
  public coupon = true;
  public Blog = true;
  public Banner = true;
  public order = true;
  public payment = true;
  public content = true;
  public faq = true;
  public vacancies = true;
  public subscribers = true

  constructor() { }

  ngOnInit(): void {
  }

}
