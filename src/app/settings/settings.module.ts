import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { AddUpdateSettingsComponent } from './add-update-settings/add-update-settings.component';
import { ViewSettingsComponent } from './view-settings/view-settings.component';

import { SettingsService } from './settings.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ImageUploadModule } from 'ng2-imageupload';
import { CKEditorModule } from 'ng2-ckeditor';


@NgModule({
  declarations: [AddUpdateSettingsComponent, ViewSettingsComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ImageUploadModule,
    NgxDatatableModule,
    CKEditorModule
  ],
  providers: [SettingsService]
})
export class SettingsModule { }
