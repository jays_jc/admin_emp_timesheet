import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUpdateSettingsComponent } from './add-update-settings.component';

describe('AddUpdateSettingsComponent', () => {
  let component: AddUpdateSettingsComponent;
  let fixture: ComponentFixture<AddUpdateSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUpdateSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUpdateSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
