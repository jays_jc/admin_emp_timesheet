import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { SettingsService } from '../settings.service';
import { ToastrService } from 'ngx-toastr';
import { reduce } from 'rxjs/operators';
import { ImageResult, ResizeOptions } from 'ng2-imageupload';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-add-update-settings',
  templateUrl: './add-update-settings.component.html',
  styleUrls: ['./add-update-settings.component.scss']
})
export class AddUpdateSettingsComponent implements OnInit {
  @ViewChild('myInput', { static: false }) myInputVariable: any;
  public settForm: FormGroup;
  public ID: any;
  public _host = environment.url;
  submitted = false;
  _settObservable: any;
  public tab = 'commission';
  modalimg = '';
  imageModal = false;

  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private _route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private setService: SettingsService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal) {
    this.createForm();
  }


  bannerImage = []

  ngOnInit(): void {
    this.ID = this._activateRouter.snapshot.params['id'];

    if (localStorage.getItem('tab')) {
      this.tab = localStorage.getItem('tab')
    }

    this.spinner.show();
    this.setService.get().subscribe((res: any) => {
      if (res.success) {
        this.settForm.patchValue({
          commission: res.data.commission,
          gst: res.data.gst,
          discount: res.data.discount,
          delivery_charges: res.data.delivery_charges,
          bannerTitle: res.data.bannerTitle,
          bannerDiscription: res.data.bannerDiscription,
          bannerImages: res.data.bannerImages
        });
        this.bannerImage = res.data.bannerImages
      } else {
        this.toastr.error(res.error.message, 'Error');
      }
      this.spinner.hide();
    },
      error => {
        this.spinner.hide();
        // this.toastr.error(error, 'Error');
      });

  }

  createForm() {
    this.settForm = this.formBuilder.group({
      commission: ['', Validators.required],
      gst: ['', Validators.required],
      discount: ['', Validators.required],
      delivery_charges: ['', Validators.required],
      bannerTitle: ['', Validators.required],
      bannerDiscription: ['', Validators.required],
      bannerImages: ['', Validators.required],
    });
  }

  get f() { return this.settForm.controls; }

  modalOpen(p) {
    this.imageModal = true;
    this.modalimg = p;
  }

  modalClose() {
    this.imageModal = false;
    this.modalimg = '';
  }

  uploadImage(imageResult: ImageResult) {

    if (this.bannerImage.length >= 5) {
      this.toastr.warning("You Can't upload more than 5 images", 'Warning');
      return false;
    }
    let object = {
      data: imageResult.dataURL,
      type: 'banner'
    }
    this.myInputVariable.nativeElement.value = "";
    this.spinner.show();
    this.setService.uploadImage(object).subscribe((res: any) => {
      if (res.success) {

        this.bannerImage.push(res.data.imagePath);
        this.settForm.controls.bannerImages.setValue(this.bannerImage);
      } else {
        window.scrollTo(0, 0);
        this.toastr.error(res.error.message, 'Error');
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastr.error('There are some errors, please try again after some time !', 'Error');
    });
  }

  removeImage(i) {
    if (window.confirm('Do you want to delete this?')) {
      this.bannerImage = this.bannerImage.filter((item, j) => i !== j);
      this.settForm.controls.bannerImages.setValue(this.bannerImage);
    }
  }

  onSubmit() {
    this.submitted = true;
    if (!this.settForm.invalid) {
      this.spinner.show();

      this._settObservable = this.setService.update(this.settForm.value, this.ID).subscribe(res => {
        let url;
        if (res.success) {
          this.toastr.success('Settings updated successfully!');
          url = '/settings';
          this.router.navigate([url]);
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });

    }
  }

  ngOnDestroy(): void {
    if (this._settObservable) {
      this._settObservable.unsubscribe();
    }
  }




}
