import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddUpdateSettingsComponent } from './add-update-settings/add-update-settings.component';
import { ViewSettingsComponent } from './view-settings/view-settings.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ViewSettingsComponent
      },
      {
        path: 'view',
        component: ViewSettingsComponent
      },
      {
        path: 'add',
        component: AddUpdateSettingsComponent
      },
      {
        path: 'edit/:id',
        component: AddUpdateSettingsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
