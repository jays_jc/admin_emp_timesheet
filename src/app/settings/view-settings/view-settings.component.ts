import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { SettingsService } from '../settings.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-view-settings',
  templateUrl: './view-settings.component.html',
  styleUrls: ['./view-settings.component.scss']
})
export class ViewSettingsComponent implements OnInit {

  public setting: any = {};
  public ID: any;
  conditionalForm: boolean = false;
  public id: any;
  public tab = 'commission';
  public bannerForm: FormGroup;
  submitted = false;
  _bannerObservable: any;
  bannerImage = ''
  public _host = environment.url;

  constructor(
    private router: Router,
    private _route: ActivatedRoute,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private setService: SettingsService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal) {
    this.createForm();
  }

  ngOnInit(): void {

    if (localStorage.getItem('tab')) {
      this.tab = localStorage.getItem('tab')
    }

    this.spinner.show();
    this.setService.get().subscribe((res: any) => {
      if (res.success) {
        this.setting = res.data;
        this.ID = res.data.id;
      } else {
        this.toastr.error(res.error.message, 'Error');
      }
      this.spinner.hide();
    },
      error => {
        this.spinner.hide();
        this.toastr.error(error, 'Error');
      });

  }


  createForm() {
    this.bannerForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      image: ['', Validators.required]
    });
  }

  get f() { return this.bannerForm.controls; }


  edit(tab) {
    let route = '/settings/edit/' + this.ID + '&tab=' + tab;
    localStorage.setItem("tab", tab)
    this.router.navigate([route]);
  }

  ngOnDestroy(): void {
    if (this._bannerObservable) {
      this._bannerObservable.unsubscribe();
    }
  }
}
