  import { Component, OnInit } from '@angular/core';
  import { Router, ActivatedRoute } from '@angular/router';
  import { NgxSpinnerService } from 'ngx-spinner';
  import { PaymentsService } from '../payments.service';
  import { ToastrService } from 'ngx-toastr';
  import { environment } from '../../../environments/environment';
  declare let jsPDF;
  
  @Component({
    selector: 'app-view-payment',
    templateUrl: './view-payment.component.html',
    styleUrls: ['./view-payment.component.css']
  })
  export class ViewPaymentComponent implements OnInit {
  
    public payID: any;
    public pay: any = {
      shippingDetail: {},
      billingDetail: {}
    };
    
    conditionalForm: boolean = false;
    _host = environment.url;
    invoice = false;
  
    constructor(
      private router : Router,
      private _activateRouter: ActivatedRoute,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService,
      private payService: PaymentsService ) { }
  
    ngOnInit(): void {
      this.payID = this._activateRouter.snapshot.params['id']; 
      
      if(this.payID){
        this.conditionalForm = true;
        this.spinner.show();
        this.payService.get(this.payID).subscribe((res: any) => {
          if (res.success) {
            this.pay = res.transcations;
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
      }else{
       this.conditionalForm = false;
      }
    }


    generateInvoice() {
      this.spinner.show();
      this.invoice = true;
      setTimeout(() => {
        let elementToPrint = document.getElementById('invoice');
        let pdf = new jsPDF('p', 'pt', 'a4');
        pdf.addHTML(elementToPrint, () => {
          let fileName = "Invoice";
          pdf.save(fileName);
          setTimeout(() => {
            this.spinner.hide();
            this.invoice = false;
          }, 1);
        });
      }, 1);
    }
  
  
  
  }
