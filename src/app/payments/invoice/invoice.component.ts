import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  @Input() pay = {
    shippingDetail:{},
    billingDetail:{}
  }
  constructor() { }

  ngOnInit() {
  }

}
