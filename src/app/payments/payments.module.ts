import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentsService } from './payments.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxSpinnerModule } from 'ngx-spinner';
import { InvoiceComponent } from './invoice/invoice.component';

import { PaymentsRoutingModule } from './payments-routing.module';
import { ListPaymentsComponent } from './list-payments/list-payments.component';
import { ViewPaymentComponent } from './view-payment/view-payment.component';


@NgModule({
  declarations: [ListPaymentsComponent, InvoiceComponent, ViewPaymentComponent],
  imports: [
    CommonModule,
    PaymentsRoutingModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule
  ],
  providers: [PaymentsService]
})
export class PaymentsModule { }
