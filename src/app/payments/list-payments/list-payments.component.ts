  import { Component, OnInit } from '@angular/core';
  import { PaymentsService } from '../payments.service';
  import { NgxSpinnerService } from 'ngx-spinner';
  import { ToastrService } from 'ngx-toastr';
  import { ColumnMode } from '@swimlane/ngx-datatable';
  import { Router } from '@angular/router';
  
  @Component({
    selector: 'app-list-payments',
    templateUrl: './list-payments.component.html',
    styleUrls: ['./list-payments.component.css']
  })
  export class ListPaymentsComponent implements OnInit {
  
    public payData: Array<any> = [];
    public response: any;
  
    rows = [];
    columns = [];
    ColumnMode = ColumnMode;
    _subscriberData: any;
    isLoading: boolean = false;
    page = 0;
    filters: { page: number; search: string } = { page: 2, search: '' };
  
    constructor(
      private toastr: ToastrService,
      private spinner: NgxSpinnerService,
      private payService: PaymentsService,
      private router: Router
    ) { }
  
    ngOnInit(): void {
      this.getPays();
    }
  
    view(ID) {
      let route = '/payments/list/' + ID;
      this.router.navigate([route]);
    }
  
    getPays() {
      this.isLoading = true;
      this.spinner.show()
      this._subscriberData = this.payService.getAllPayments(this.filters).subscribe((response) => {
        if (response.transcations.length == 0) {
          this.payData = [];
          this.isLoading = false;
          this.spinner.hide()
        } else {
          this.payData = response.transcations.map(data => {
            return {
              id: data.id,
              tID: data.transaction_id,
              price: data.price,
              date: data.createdAt,
              receipt: data.detail.receipt_url,
              status: data.payment_status
            }
          });
          this.isLoading = false;
          this.spinner.hide()
        }
      });
    }

  
    setPage(e) {
      console.log(e);
      this.page = e.offset + 1;
      Object.assign(this.filters, { page: this.page });
      this.getPays();
    }
  
    searchValue() {
      this.page = 0;
      Object.assign(this.filters, { page: this.page, search: this.filters.search });
      this.getPays();
    }
  
    clearValue() {
      this.page = 0;
      this.filters.search = '';
      Object.assign(this.filters, { page: this.page, search: this.filters.search });
      this.getPays();
    }
  
  
  
  }
