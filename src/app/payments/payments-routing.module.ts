import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPaymentsComponent } from './list-payments/list-payments.component';
import { ViewPaymentComponent } from './view-payment/view-payment.component';

const routes: Routes = [
  {
    path: '',
    children: [
     {
        path: '',
        component: ListPaymentsComponent
      },
      {
        path: 'list',
        component: ListPaymentsComponent
      },
      {
        path: 'list/:id',
        component: ViewPaymentComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule { }
