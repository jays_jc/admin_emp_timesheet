import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ProfileService } from './profile.service';
import { ProfileRoutingModule } from './profile-routing.module';
import { MyprofileComponent } from './myprofile-component/myprofile.component';
import { EditprofileComponentComponent } from './editprofile-component/editprofile-component.component';
import { ImageUploadModule } from 'ng2-imageupload';

@NgModule({
  declarations: [ MyprofileComponent,
    EditprofileComponentComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    NgxSpinnerModule,
    NgbModule,
    ImageUploadModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ProfileService]
})
export class ProfileModule { }
