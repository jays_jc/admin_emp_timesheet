import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProfileService } from '../profile.service';
import { BehaviorService } from "../../shared/behavior.service";
import { ToastrService } from 'ngx-toastr';
import { ImageResult, ResizeOptions } from 'ng2-imageupload';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-editprofile-component',
  templateUrl: './editprofile-component.component.html',
  styleUrls: ['./editprofile-component.component.scss']
})
export class EditprofileComponentComponent implements OnInit {

  @ViewChild('myInput', { static: false })
  myInputVariable: any;

  public _host = environment.url;
  public userID: any;
  public user: any = {};
  public response: any;
  conditionalForm: boolean = false;
  public userImage = '';

  public userForm: FormGroup;
  submitted = false;
  _userObservable: any;


  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private profileService: ProfileService,
    public _bs: BehaviorService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    if (this.user) {
      this.userID = this.user.id;
    }

    if (this.user) {
      this.conditionalForm = true;

      this.userForm.patchValue({
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        email: this.user.email,
        role: this.user.role,
        mobileNo: this.user.mobileNo,
        image: this.user.image
      });
      this.userImage = this.user.image;
    } else {
      this.conditionalForm = false;
    }

  }

  createForm() {
    this.userForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      role: ['SA'],
      email: ['', [Validators.required, Validators.email]],
      mobileNo: ['', [Validators.required, Validators.min(1000000000), Validators.max(999999999999), Validators.pattern]],
      image: ['']
    });
  }

  get f() { return this.userForm.controls; }


  onSubmit() {
    this.submitted = true;
    if (!this.userForm.invalid) {
      this.spinner.show();

      this._userObservable = this.profileService.update(this.userForm.value, this.userID).subscribe(res => {
        let url;
        if (res.success) {
          this.user.firstName = this.userForm.value.firstName
          this.user.lastName = this.userForm.value.lastName
          this.user.mobileNo = this.userForm.value.mobileNo
          this.user.image = this.userForm.value.image
          const storage = localStorage;
          storage.setItem('user', JSON.stringify(this.user));
          this._bs.setUserData(this.user);
          this.toastr.success('Profile Updated Successfully!');
          url = '/profile';
          this.router.navigate([url]);
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
    }
  }

  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'users'
    }

    console.log(object)
    this.myInputVariable.nativeElement.value = "";
    this.spinner.show();
    this.profileService.uploadImage(object).subscribe((res: any) => {
      if (res.success) {
        this.userImage = res.data.imagePath;
        this.userForm.patchValue({ image: this.userImage })
      } else {
        window.scrollTo(0, 0);
        this.toastr.error(res.error.message, 'Error');
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastr.error('There are some errors, please try again after some time !', 'Error');
    });
  }

  removeImage() {
    this.userForm.controls.image.setValue('');
    this.userImage = '';
  }

  ngOnDestroy(): void {
    if (this._userObservable) {
      this._userObservable.unsubscribe();
    }
  }


}
