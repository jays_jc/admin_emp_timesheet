  import { Component, OnInit } from '@angular/core';
  import { Router, ActivatedRoute } from '@angular/router';
  import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
  import { FormGroup, FormBuilder, Validators } from '@angular/forms';
  import { NgxSpinnerService } from 'ngx-spinner';
  import { ProfileService } from '../profile.service';
  import { BehaviorService } from "../../shared/behavior.service";
  import { ToastrService } from 'ngx-toastr';
  
  @Component({
    selector: 'app-myprofile',
    templateUrl: 'myprofile.component.html'
  })
  export class MyprofileComponent implements OnInit {
  
    public userID: any;
    public user: any = {};
  
    constructor(
      private router : Router,
      private _activateRouter: ActivatedRoute,
      private toastr: ToastrService,
      private spinner: NgxSpinnerService,
      private profileService: ProfileService,
      public _bs: BehaviorService,
      private formBuilder: FormBuilder,
      private modalService: NgbModal ) { }
  
    ngOnInit(): void {
      this._bs.getUserData().subscribe((res: any) => {
        this.user = res;
      });

      
      this.user = JSON.parse(localStorage.getItem('user'));
      if (this.user) {
        this.userID = this.user.id;
      }

    }

    edit() {
      let route = '/profile/edit';
      this.router.navigate([route]);
    }
  
  
  
  }
  