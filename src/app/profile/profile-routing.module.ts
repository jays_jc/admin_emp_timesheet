import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyprofileComponent } from './myprofile-component/myprofile.component';
import { EditprofileComponentComponent } from './editprofile-component/editprofile-component.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Profile'
    },
    children: [
      {
        path: '',
        component: MyprofileComponent,
        data: {
          title: 'My Profile'
        }
      },
      {
        path: 'myprofile',
        component: MyprofileComponent,
        data: {
          title: 'My Profile'
        }
      },
      {
        path: 'edit',
        component: EditprofileComponentComponent,
        data: {
          title: 'Edit Profile'
        }
      }   
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
