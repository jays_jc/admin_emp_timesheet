import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { BlogsService } from '../blogs.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { ColumnMode } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-view-blog',
  templateUrl: './view-blog.component.html',
  styleUrls: ['./view-blog.component.scss']
})
export class ViewBlogComponent implements OnInit {
  pageNo: any;
  public _host = environment.url;

  public blogID: any;
  public blog: any = {};
  conditionalForm: boolean = false;
  public popupImage = '';

  rows = [];
  columns = [];
  ColumnMode = ColumnMode;
  isLoading: boolean = false;
  comments = []
  page = 0;
  isDeleted = false
  filters: { page: number; search: string, isDeleted: Boolean } = { page: 1, search: '', isDeleted: false };
  actionType = 'active';
  public Data: Array<any> = [];
  public response: any;
  public response2: any;

  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private blogService: BlogsService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal) { }

  ngOnInit(): void {


    // this.Data = [
    //   { title: 'gffg', email: "mk@yopmail.com", message: 'fgfgf' }
    // ]


    this.pageNo = this._activateRouter.snapshot.params['page'];

    this.blogID = this._activateRouter.snapshot.params['id'];

    if (this.blogID) {
      this.conditionalForm = true;
      this.spinner.show();
      this.blogService.get(this.blogID).subscribe((res: any) => {
        if (res.success) {
          this.blog = res.data;
          this.comments = res.data.comments
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
    } else {
      this.conditionalForm = false;
    }
  }

  openModal(img) {
    this.popupImage = img;
  }


  view(ID) {
    // this.feedbackService.actionType.next(type)
    // this.feedbackService.activePage.next(this.page)
    // let route = '/feedback/edit/' + ID+'/'+this.page;
    let route = '/comments/view/' + ID;
    this.router.navigate([route]);
  }

  reply(ID, type) {
    // this.feedbackService.actionType.next(type)
    // this.feedbackService.activePage.next(this.page)
    // let route = '/categoryType/edit/' + ID+'/'+this.page;
    let route = '/comments/reply/' + ID
    this.router.navigate([route]);
  }

  /* Function use to remove Crop with crop id */
  remove(ID) {
    if (confirm("Do you want to delete this feedback type?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'feedback'
      }
    }
  }



}
