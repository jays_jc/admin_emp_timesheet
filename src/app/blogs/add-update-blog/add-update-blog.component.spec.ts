import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUpdateBlogComponent } from './add-update-blog.component';

describe('AddUpdateBlogComponent', () => {
  let component: AddUpdateBlogComponent;
  let fixture: ComponentFixture<AddUpdateBlogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUpdateBlogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUpdateBlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
