import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { BlogsService } from '../blogs.service';
import { ToastrService } from 'ngx-toastr';
import { ImageResult, ResizeOptions } from 'ng2-imageupload';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-add-update-blog',
  templateUrl: './add-update-blog.component.html',
  styleUrls: ['./add-update-blog.component.scss']
})
export class AddUpdateBlogComponent implements OnInit {
  pageNo:any;
  @ViewChild('myInput', { static: false })
  myInputVariable: any;

  public blogID: any;
  public blogImage = '';
  public token = '';
  public types: any = [];
  public images: any = [];
  public response: any;
  public afuConfig: any = {};
  public data: any = {};
  conditionalForm: boolean = false;
  public _host = environment.url;

  public blogForm: FormGroup;
  submitted = false;
  _blogObservable: any;

  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private blogService: BlogsService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
  ) {
    this.pageNo = this._activateRouter.snapshot.params['page'];
    this.createForm();
  }

  ngOnInit(): void {
    this.token = localStorage.getItem('token');

    this.getCatTypes();

    this.blogID = this._activateRouter.snapshot.params['id'];

    if (this.blogID) {
      this.conditionalForm = true;
      this.spinner.show();
      this.blogService.get(this.blogID).subscribe((res: any) => {
        if (res.code == 200) {
          this.blogForm.patchValue({
            title: res.data.title,
            description: res.data.description,
            categoryID: res.data.categoryID.id,
            image: res.data.image
          });
          this.blogImage = res.data.image;
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          this.toastr.error(error, 'Error');
        });
    } else {
      this.conditionalForm = false;
    }

  }

  createForm() {
    this.blogForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      categoryID: ['', Validators.required],
      image: ['']
    });
  }

  get f() { return this.blogForm.controls; }


  getCatTypes() {
    let data = {
      status: "active"
    }
    this.blogService.getTypes(data).subscribe(res => {
      if (res.success) {
        for (let i = 0; i < res.data.length; i++) {
          if (res.data[i].type && res.data[i].type.name == "Blog") {
            this.types.push(res.data[i]);

          }
        }
      } else {
        this.toastr.error(res.message);
      }
    },
      error => {
        this.toastr.error(error);
      }
    )
  }

  onSubmit() {
    this.submitted = true;
    if (!this.blogImage) {
      return
    }
    if (!this.blogForm.invalid) {
      this.spinner.show();
      let data = this.blogForm.value;
      data.image = this.blogImage

      if (this.conditionalForm) {
        this._blogObservable = this.blogService.update(data, this.blogID).subscribe(res => {
          let url;
          if (res.code == 200) {
            this.toastr.success('Blog updated successfully!');
            url = '/blogs/blogs/'+this.pageNo;
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            this.toastr.error(error, 'Error');
          });

      } else {
        this._blogObservable = this.blogService.add(data).subscribe(res => {
          let url;
          if (res.code == 200) {
            this.toastr.success('Blog added successfully!');
            url = '/blogs';
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          });
      }
    }
  }


  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'blogs'
    }
    this.myInputVariable.nativeElement.value = "";
    this.spinner.show();
    this.blogService.uploadImage(object).subscribe((res: any) => {
      if (res.success) {
        this.blogImage = res.data.imagePath;
      } else {
        window.scrollTo(0, 0);
        this.toastr.error(res.error.message, 'Error');
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastr.error('There are some errors, please try again after some time !', 'Error');
    });
  }

  removeImage() {
    this.blogForm.controls.image.setValue('');
    this.blogImage = '';
  }

  ngOnDestroy(): void {
    if (this._blogObservable) {
      this._blogObservable.unsubscribe();
    }
  }




}