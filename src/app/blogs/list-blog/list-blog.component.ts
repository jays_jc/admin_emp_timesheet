import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { BlogsService } from '../blogs.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-blog',
  templateUrl: './list-blog.component.html',
  styleUrls: ['./list-blog.component.scss']
})
export class ListBlogComponent implements OnInit {
  actionType = 'active'
  public blogsData: Array<any> = [];
  public response: any;
  public response2: any;

  rows = [];
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  isDeleted = false
  filters: { page: number; search: string, isDeleted: Boolean } = { page: 1, search: '', isDeleted: false };


  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private blogService: BlogsService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router,
    private _activateRouter:ActivatedRoute
  ) { 
    this.blogService.actionType.subscribe(res=>{ 
      if(res!=='' && res!==undefined && res!==null){
        this.actionType = res
        if (this.actionType == 'active') {
          this.filters.isDeleted = false;

        } else if (this.actionType == 'deleted') {
          this.filters.isDeleted = true;
        }
      } else {
        this.actionType = 'active'
      }
    })
    console.log('this.actionType',this.actionType)
   }

  ngOnInit(): void {
    if(this._activateRouter.snapshot.params['page']){
     
      this.filters.page = JSON.parse(this._activateRouter.snapshot.params['page'])  ;
      this.page  = this.filters.page;
      console.log(this.filters.page,typeof(this._activateRouter.snapshot.params['page']))
      Object.assign(this.filters, { page: this.filters.page });  
      this.getBlogs(); 
    }else{ 
      this.page = 1
      this.getBlogs(); 
    } 
  }
  add() {
    let route = '/blogs/add/';
    this.blogService.actionType.next('active')
    this.router.navigate([route]);
  }
  edit(ID, type) {
    this.blogService.actionType.next(type)
    let route = '/blogs/edit/' + ID+"/"+this.page;
    this.router.navigate([route]);
  }

  view(ID, type) {
    this.blogService.actionType.next(type)
    let route = '/blogs/list/' + ID+ "/"+this.page;
    this.router.navigate([route]);
  }

  /* Function use to remove Crop with crop id */
  remove(ID) {
    if (confirm("Do you want to delete this blog ?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'blogs'
      }

      this.blogService.deleteRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Blog Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getBlogs();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  /* Function use to remove Crop with crop id */
  removePermanent(ID) {
    if (confirm("Do you want to delete this blog permanently?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'blogs'
      }

      this.blogService.deleteRecordPermanent(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Blog Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getBlogs();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  /* Function use to remove Crop with crop id */
  moveBack(ID) {
    if (confirm("Do you want to move back this blog to listing ?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'blogs'
      }

      this.blogService.moveBackRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Blog Moved Back Successfully');
        } else {
          this.toastr.error('Unable to move at the moment, Please try again later', 'Error');
        }
        this.getBlogs();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  getBlogs() {
    this.spinner.show();
    this.isLoading = true;
    this._subscriberData = this.blogService.getAllBlogs(this.filters).subscribe((response) => {

      if (response.data.length == 0) {
        this.blogsData = [];
        this.isLoading = false;
        this.spinner.hide();
      } else {
        this.blogsData = response.data
        this.isLoading = false;
        this.spinner.hide();
      }
    });
  }

  changeStatus(data, ID) {
    let Status = '';
    let message = 'Do you want to activate this blog ?';
    if (data == 'active') {
      Status = "deactive";
      message = 'Do you want to deactivate this blog ?';
    } else {
      Status = "active";
    }


    if (confirm(message)) {
      this.spinner.show();
      this.blogService.status(ID, 'blogs', Status).subscribe((res: any) => {
        if (res.success) {
          this.response2 = res;
          this.toastr.success(res.message);
          this.getBlogs();
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  setPage(e) {
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    let route = '/blogs/blogs/' + this.page;
    this.router.navigate([route]);
    this.getBlogs();
  }

  searchValue() {
    this.page = 1;
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getBlogs();
  }

  clearValue() {
    this.page = 1;
    this.filters.search = '';
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getBlogs();
  }



}
