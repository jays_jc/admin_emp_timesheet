import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogsRoutingModule } from './blogs-routing.module';
import { AddUpdateBlogComponent } from './add-update-blog/add-update-blog.component';
import { ListBlogComponent } from './list-blog/list-blog.component';
import { ViewBlogComponent } from './view-blog/view-blog.component';

import { NgxSpinnerModule } from 'ngx-spinner';
import { BlogsService } from './blogs.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CKEditorModule } from 'ng2-ckeditor';
import { AuthInterceptor } from '../shared/auth-interceptor';
import { ImageUploadModule } from 'ng2-imageupload';

@NgModule({
  declarations: [AddUpdateBlogComponent, ListBlogComponent, ViewBlogComponent],
  imports: [
    CommonModule,
    BlogsRoutingModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
    ImageUploadModule,
    CKEditorModule,
  ],
  providers: [
    BlogsService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
})
export class BlogsModule { }
