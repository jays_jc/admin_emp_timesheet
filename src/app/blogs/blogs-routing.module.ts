import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddUpdateBlogComponent } from './add-update-blog/add-update-blog.component';
import { ListBlogComponent } from './list-blog/list-blog.component';
import { ViewBlogComponent } from './view-blog/view-blog.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Blogs'
    },
    children: [
      {
        path: '',
        component: ListBlogComponent
      },{
        path: 'blogs/:page',
        component: ListBlogComponent
      },
      {
        path: 'list',
        component: ListBlogComponent
      },
      {
        path: 'list/:id/:page',
        component: ViewBlogComponent
      },
      {
        path: 'add',
        component: AddUpdateBlogComponent
      },
      {
        path: 'edit/:id',
        component: AddUpdateBlogComponent
      },{
        path: 'edit/:id/:page',
        component: AddUpdateBlogComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogsRoutingModule { }
