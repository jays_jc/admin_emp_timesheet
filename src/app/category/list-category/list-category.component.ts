import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss']
})
export class ListCategoryComponent implements OnInit {
  actionType = 'active'
  public catData: Array<any> = [];
  public response: any;
  public response2: any;

  rows = [];
  columns = [];
  ColumnMode = ColumnMode;
  _subscriberData: any;
  isLoading: boolean = false;
  page = 0;
  isDeleted = false
  totalItems = 0
  // filters: { page: number; search: string, isDeleted: boolean } = { page: 1, search: '', isDeleted: false };
  filters: {
    page: number;
    count: number;
    search: string;
    sortBy: string;
    date: string;
    // isDeleted: boolean
  } = {
      page: 1,
      count: 5,
      search: '',
      sortBy: 'createdAt desc',
      date: new Date().getTime().toString(),
      // isDeleted: false
    };
  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private catService: CategoryService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private router: Router,
    private _activateRouter: ActivatedRoute
  ) {
    if (this._activateRouter.snapshot.params['page']) {

      this.filters.page = JSON.parse(this._activateRouter.snapshot.params['page']);
      this.page = this.filters.page;
      console.log(this.filters.page, typeof (this._activateRouter.snapshot.params['page']))
      Object.assign(this.filters, { page: this.filters.page });
      this.getCategory();
    } else {
      this.page = 1
      this.getCategory();
    }
    this.catService.actionType.subscribe(res => {
      if (res !== '' && res !== undefined && res !== null) {
        this.actionType = res
        if (this.actionType == 'active') {
          // this.filters.isDeleted = false;

        } else if (this.actionType == 'deleted') {
          Object.assign(this.filters, { isDeleted: true });
          // this.filters.isDeleted = true;
        }
      } else {
        this.actionType = 'active'
      }
    })
    console.log('this.actionType0', this.actionType)
  }

  ngOnInit(): void {

  }
  add() {
    let route = '/category/add/';
    this.catService.actionType.next('active')
    this.router.navigate([route]);
  }
  edit(ID) {
    this.catService.actionType.next('active')
    let route = '/category/edit/' + ID + "/" + this.page;
    this.router.navigate([route]);
  }

  /* Function use to remove Crop with crop id */
  remove(ID) {
    if (confirm("Do you want to delete this category?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'category'
      }

      this.catService.deleteRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Category Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getCategory();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  /* Function use to remove Crop with crop id */
  removePermanent(ID) {
    if (confirm("Do you want to delete this category permanently?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'category'
      }

      this.catService.deleteRecordPermanent(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Category Deleted Successfully');
        } else {
          this.toastr.error('Unable to delete at the moment, Please try again later', 'Error');
        }
        this.getCategory();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  /* Function use to remove Crop with crop id */
  moveBack(ID) {
    if (confirm("Do you want to move back this category to listing ?")) {
      this.spinner.show();

      let obj = {
        id: ID,
        model: 'category'
      }

      this.catService.moveBackRecord(obj).subscribe((res: any) => {
        if (res.success) {
          this.response = res;
          this.toastr.success('Category Moved Back Successfully');
        } else {
          this.toastr.error('Unable to move at the moment, Please try again later', 'Error');
        }
        this.getCategory();
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  getCategory() {
    this.spinner.show();
    console.log(this.actionType, "this.actionType")
    if (this.actionType == 'active') {
      console.log("in if")
      delete this.filters["isDeleted"]
    } else if (this.actionType == 'deleted') {
      console.log("in else")
      Object.assign(this.filters, { isDeleted: true });
    }
    this._subscriberData = this.catService.getAllCategory(this.filters).subscribe((response) => {
      if (response['data'].length == 0) {
        this.catData = [];
        this.spinner.hide();
        // this.isLoading = false;
      } else {
        this.catData = response['data'].map(cat => {
          console.log(cat, "category")
          // if (cat && cat.parentCategory && cat.parentCategory.name) {
          //   return {
          //     id: cat.id,
          //     catName: cat.name,
          //     date: cat.createdAt,
          //     status: cat.status,
          //     catParent: cat.parentCategory.name
          //   }
          // }

          // if (cat && cat.type && cat.type.name) {
          //   return {
          //     id: cat.id,
          //     catName: cat.name,
          //     date: cat.createdAt,
          //     status: cat.status,
          //     catType: cat.type.name
          //   }
          // }

          if (cat && cat.parentCategory && cat.parentCategory.name) {
            return {
              id: cat.id,
              catName: cat.name,
              date: cat.createdAt,
              deletedBy: cat.deletedBy,
              status: cat.status,
              catParent: cat.parentCategory.name
            }
          }

          // if (cat && cat.sub_parent_id && cat.sub_parent_id.name) {
          //   return {
          //     id: cat.id,
          //     catName: cat.name,
          //     date: cat.createdAt,
          //     status: cat.status,
          //     catSubParent: cat.sub_parent_id.name
          //   }
          // }

          return {
            id: cat.id,
            catName: cat.name,
            date: cat.createdAt,
            deletedBy: cat.deletedBy,
            status: cat.status
          }
        });
        this.totalItems = response.total;
        this.spinner.hide();
      }
    });
  }

  changeStatus(data, ID) {
    let Status = '';
    let message = 'Do you want to activate this category ?';
    if (data == 'active') {
      Status = "deactive";
      message = 'Do you want to deactivate this category ?';
    } else {
      Status = "active";
    }


    if (confirm(message)) {
      this.spinner.show();
      this.catService.status(ID, 'category', Status).subscribe((res: any) => {
        if (res.success) {
          this.response2 = res;
          this.toastr.success(res.message);
          this.getCategory();
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastr.error('There are some errors, please try again after some time !', 'Error');
      });
    }
  }

  // setPage(e) {
  //   console.log(e);
  //   this.page = e.offset + 1;
  //   Object.assign(this.filters, { page: this.page });
  //   let route = '/category/category/' + this.page;
  //   this.router.navigate([route]);
  //   this.getCategory();
  // }

  // searchValue() {
  //   this.page = 1;
  //   Object.assign(this.filters, { page: this.page, search: this.filters.search });
  //   this.getCategory();
  // }

  // clearValue() {
  //   this.page = 1;
  //   this.filters.search = '';
  //   Object.assign(this.filters, { page: this.page, search: this.filters.search });
  //   this.getCategory();
  // }

  setPage(e) {
    console.log(e);
    this.page = e.offset + 1;
    Object.assign(this.filters, { page: this.page });
    // let route = '/order/list/' + this.page;
    this.getCategory();
    // this.router.navigate([route]);
  }

  searchValue() {
    this.page = 1;
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getCategory();
  }

  clearValue() {
    this.page = 1;
    this.filters.search = '';
    Object.assign(this.filters, { page: this.page, search: this.filters.search });
    this.getCategory();
  }



}
