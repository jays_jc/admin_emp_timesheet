import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CategoryService } from '../category.service';
import { ToastrService } from 'ngx-toastr';
import { ImageResult, ResizeOptions } from 'ng2-imageupload';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-add-update-category',
  templateUrl: './add-update-category.component.html',
  styleUrls: ['./add-update-category.component.scss']
})
export class AddUpdateCategoryComponent implements OnInit {

  @ViewChild('myInput', { static: false })
  myInputVariable: any;

  public _host = environment.url;
  public categoryID: any;
  public types: any = [];
  public catImage = '';
  public response: any;
  conditionalForm: boolean = false;
  showParentCatType: boolean = true;
  public ParentCategory = true;
  public SubParentCategory = true;

  public categoryForm: FormGroup;
  submitted = false;
  _categoryObservable: any;

  public parentCats: any = [];
  public subParentCats: any = [];
  selectedSubParentcategory = '';


  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private catService: CategoryService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    // this.getCatTypes();
    this.getParentCats();

    this.categoryID = this._activateRouter.snapshot.params['id'];

    if (this.categoryID) {
      this.conditionalForm = true;
      this.spinner.show();
      this.catService.getCat(this.categoryID).subscribe((res: any) => {

        if (res.code == 200) {
          this.categoryForm.patchValue({
            name: res.data.name,
            // parentCategory: res.data.parentCategory
          });
          // if (res.category && res.category.type && res.category.type.id) {
          //   this.showParentCatType = true;
          //   this.categoryForm.controls.type.setValue(res.category.type.id);
          // }

          if (res.data && res.data.parentCategory && res.data.parentCategory.id) {
            this.showParentCatType = false;
            this.categoryForm.controls.parentCategory.setValue(res.data.parentCategory.id);
            this.getParentCats()
          }

          // if (res.category && res.category.sub_parent_id && res.category.sub_parent_id.id) {
          //   this.showParentCatType = false;
          //   this.categoryForm.controls.sub_parent_id.setValue(res.category.sub_parent_id.id);
          // }

          // if (res.category && res.category.parentCategory && res.category.parentCategory.id) {
          //   this.getSubParent(res.category.parentCategory.id)

          //   if (res.category && res.category.sub_parent_id && res.category.sub_parent_id.id) {
          //     this.selectedSubParentcategory = res.category.sub_parent_id.id;
          //     this.categoryForm.controls.sub_parent_id.setValue(this.selectedSubParentcategory);
          //   }
          // }
          // this.catImage = res.category.image;
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          this.toastr.error(error, 'Error');
        });
    } else {
      this.conditionalForm = false;
    }

  }

  createForm() {
    this.categoryForm = this.formBuilder.group({
      name: ['', Validators.required],
      parentCategory: ['']
      // sub_parent_id: [''],
      // type: [''],
      // image: ['']
    });
  }

  get f() { return this.categoryForm.controls; }


  // getCatTypes() {
  //   this.catService.getTypes().subscribe(res => {
  //     if (res.success) {
  //       this.types = res.data.CategoryTypes;

  //       //       for (let i = 0; i < this.types.length; i++) {
  //       //        if (this.types[i].name == 'Covid' || this.types[i].name == 'Blog') {
  //       //          if (this.types[i]['Value'] = false) {
  //       //           this.ParentCategory = false;
  //       //           this.SubParentCategory = false;
  //       //         }
  //       //        else {
  //       //          this.ParentCategory = true;
  //       //          this.SubParentCategory = true;
  //       //        }
  //       //     }
  //       //      else {
  //       //        this.types[i]['Value'] = true;
  //       //      }

  //       //     }

  //       console.log(this.types);
  //     } else {
  //       this.toastr.error(res.message);
  //     }
  //   },
  //     error => {
  //       this.toastr.error(error);
  //     }
  //   )
  // }

  getParentCats() {
    let data = {
      type: "parent_categories"
    }
    this.catService.getAllCategory(data).subscribe(res => {
      if (res.success) {
        this.parentCats = res.data;
      } else {
        this.toastr.error(res.message);
      }
    },
      error => {
        this.toastr.error(error);
      }
    )
  }

  getSubParent(ID) {
    if (ID) {
      this.showParentCatType = false;
      this.categoryForm.controls.type.setValue('');

      this.catService.getAllSubCategoriesOfMainCat(ID).subscribe(res => {
        if (res.success) {
          this.subParentCats = res.data.category;
        } else {
          this.toastr.error(res.message);
        }
      },
        error => {
          this.toastr.error(error);
        })
    } else {
      this.subParentCats = [];
      this.showParentCatType = true;
    }
  }

  onSubmit() {
    this.submitted = true;

    // if (this.categoryForm.controls.image.value == '') {
    //   this.toastr.warning('Please upload category image');
    //   return;
    // }

    // if (this.categoryForm.controls.parentCategory.value != '') {
    //   this.showParentCatType = false;
    //   this.categoryForm.controls.type.setValue('');
    // }


    if (!this.categoryForm.invalid) {
      this.spinner.show();
      let data = {}
      if (this.categoryForm.controls.name.value) {
        data['name'] = this.categoryForm.controls.name.value
      }
      if (this.categoryForm.controls.parentCategory.value != "") {
        data['parentCategory'] = this.categoryForm.controls.parentCategory.value
      }
      if (this.conditionalForm) {
        this._categoryObservable = this.catService.update(data, this.categoryID).subscribe(res => {
          let url;
          if (res.code == 200) {
            this.toastr.success('Category updated Successfully!');
            url = '/category';
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            this.toastr.error(error, 'Error');
          });

      } else {
        this._categoryObservable = this.catService.add(data).subscribe(res => {
          let url;
          if (res.code == 200) {
            this.toastr.success('Category saved Successfully!');
            url = '/category';
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            this.toastr.error(error, 'Error');
          });
      }
    }
  }


  uploadImage(imageResult: ImageResult) {
    let object = {
      data: imageResult.dataURL,
      type: 'category'
    }
    this.myInputVariable.nativeElement.value = "";
    this.spinner.show();
    this.catService.uploadImage(object).subscribe((res: any) => {
      if (res.success) {
        this.catImage = res.data.fullPath;
        this.categoryForm.controls.image.setValue(this.catImage);
      } else {
        window.scrollTo(0, 0);
        this.toastr.error(res.error.message, 'Error');
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastr.error('There are some errors, please try again after some time !', 'Error');
    });
  }

  removeImage(image) {
    this.spinner.show();
    this.catService.removeImage({ "Imagename": image, "type": "category" }).subscribe(res => {
      if (res.success) {
        this.categoryForm.controls.image.setValue('');
        this.catImage = '';
        this.toastr.success('Image Deleted Successfully!');
      } else {
        this.toastr.error(res.message, 'Error');
      }
      this.spinner.hide();
      return true;
    }, err => {
      this.spinner.hide();
      this.toastr.error(err.statusText, 'Error');
      return false;
    });
  }

  ngOnDestroy(): void {
    if (this._categoryObservable) {
      this._categoryObservable.unsubscribe();
    }
  } 

  filterChanged(id) {
    var value = this.types.find(x => x.id == id);
    if (value.name == "Covid" || value.name == "Blog") {
      this.ParentCategory = false;
      this.SubParentCategory = false;
    }
    else {
      this.ParentCategory = true;
      this.SubParentCategory = true;
    }
  }




}

