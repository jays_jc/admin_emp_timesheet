import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCategoryComponent } from './list-category/list-category.component';
import { AddUpdateCategoryComponent } from './add-update-category/add-update-category.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Category'
    },
    children: [
     {
        path: 'category/:page',
        component: ListCategoryComponent
      }, {
        path: '',
        component: ListCategoryComponent
      },
      {
        path: 'list',
        component: ListCategoryComponent
      },
      {
        path: 'add',
        component: AddUpdateCategoryComponent
      },
      {
        path: 'edit/:id',
        component: AddUpdateCategoryComponent
      },{
        path: 'edit/:id/:page',
        component: AddUpdateCategoryComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
