import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { ListCategoryComponent } from './list-category/list-category.component';
import { AddUpdateCategoryComponent } from './add-update-category/add-update-category.component';

import { CategoryService } from './category.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ImageUploadModule } from 'ng2-imageupload';

@NgModule({
  declarations: [ListCategoryComponent, AddUpdateCategoryComponent],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ImageUploadModule,
    NgxDatatableModule
  ],
  providers: [CategoryService]
})
export class CategoryModule { }
