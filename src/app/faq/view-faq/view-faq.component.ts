import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { FaqService } from '../faq.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-view-faq',
  templateUrl: './view-faq.component.html',
  styleUrls: ['./view-faq.component.css']
})
export class ViewFaqComponent implements OnInit {

  public _host = environment.url;

  public faqID: any;
  public faq: any = {};
  conditionalForm: boolean = false;

  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private faqService: FaqService) { }

  ngOnInit(): void {
    this.faqID = this._activateRouter.snapshot.params['id'];

    if (this.faqID) {
      this.conditionalForm = true;
      this.spinner.show();
      this.faqService.get(this.faqID).subscribe((res: any) => {
        if (res.success) {
          this.faq = res.data;
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
    } else {
      this.conditionalForm = false;
    }
  }




}
