import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUpdateFaqComponent } from './add-update-faq.component';

describe('AddUpdateFaqComponent', () => {
  let component: AddUpdateFaqComponent;
  let fixture: ComponentFixture<AddUpdateFaqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUpdateFaqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUpdateFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
