import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { FaqService } from '../faq.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-add-update-faq',
  templateUrl: './add-update-faq.component.html',
  styleUrls: ['./add-update-faq.component.css']
})
export class AddUpdateFaqComponent implements OnInit {
  pageNo:any;
  public faqID: any;
  public response: any;
  conditionalForm: boolean = false;
  public _host = environment.url;

  public faqForm: FormGroup;
  submitted = false;
  _faqObservable: any;

  constructor(
    private router: Router,
    private _activateRouter: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private faqService: FaqService,
    private formBuilder: FormBuilder,
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.pageNo = this._activateRouter.snapshot.params['page'];
    this.faqID = this._activateRouter.snapshot.params['id'];

    if (this.faqID) {
      this.conditionalForm = true;
      this.spinner.show();
      this.faqService.get(this.faqID).subscribe((res: any) => {
        if (res.code == 200) {
          this.faqForm.patchValue({
            question: res.data.question.replace(/<.*?>/g, ''),
            answer: res.data.answer.replace(/<.*?>/g, '')
          });
        } else {
          this.toastr.error(res.error.message, 'Error');
        }
        this.spinner.hide();
      },
        error => {
          this.spinner.hide();
          // this.toastr.error(error, 'Error');
        });
    } else {
      this.conditionalForm = false;
    }

  }

  createForm() {
    this.faqForm = this.formBuilder.group({
      question: ['', Validators.required],
      answer: ['', Validators.required]
    });
  }

  get f() { return this.faqForm.controls; }


  onSubmit() {
    this.submitted = true;
    if (!this.faqForm.invalid) {
      this.spinner.show();

      if (this.conditionalForm) {
        this._faqObservable = this.faqService.update(this.faqForm.value, this.faqID).subscribe(res => {
          let url;
          if (res.code == 200) {
            this.toastr.success('FAQ updated successfully!');
            url = '/faq/faq/'+this.pageNo;
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          });

      } else {
        this._faqObservable = this.faqService.add(this.faqForm.value).subscribe(res => {
          let url;
          if (res.code == 200) {
            this.toastr.success('FAQ added successfully!');
            url = '/faq';
            this.router.navigate([url]);
          } else {
            this.toastr.error(res.error.message, 'Error');
          }
          this.spinner.hide();
        },
          error => {
            this.spinner.hide();
            // this.toastr.error(error, 'Error');
          });
      }
    }
  }


  ngOnDestroy(): void {
    if (this._faqObservable) {
      this._faqObservable.unsubscribe();
    }
  }




}
