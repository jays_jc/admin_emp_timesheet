import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../users/users.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as Chart from '../../../assets/js/chart';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public users = 0;
  public jobs = 0;
  page = 0;
  filters: { page: number; search: string } = { page: 2, search: '' };
  chart = Chart[''];
  PieChart = [];

  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private userService: UsersService,
    private router: Router
  ) { }

  ngOnInit() {
    this.PieChart = new Chart('pieChart', {
      type: 'pie',
      data: {
        labels: ["Jobs", "Users", "Blogs", "Vacancies"],
        datasets: [{
          label: 'vote Now',
          data: [101, 105, 40, 50],
          backgroundColor: [
            '#01cca9', '#fb496b', '#f5cf16', '#05a8f8',
          ],
        }]
      },
      Options: {
        title: {
          text: "bar Chart",
          display: true
        },
        scales: {
          yAxes: [{
            ticks: {
              begainAtZero: true
            }
          }]
        }
      }
    });




    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep','Oct', 'Nov', 'Dec'],
        datasets: [
          {
            label: 'Application Sent',
            data: [1, 20, 55, 80, 56, 165, 95,70,130,100, 120],
            backgroundColor: '#01cca9',
            borderColor: '#01cca9',
            fill: false,
          },
          {
            label: 'Interviews',
            data: [1, 50, 85, 110, 156, 165, 105, 200, 120, 130, 50],
            backgroundColor: '#fb496b',
            borderColor: '#fb496b',
            fill: false,
          },
          {
            label: 'Rejected',
            data: [1, 50, 70, 200, 100, 180, 150, 70, 100, 130, 40],
            backgroundColor: '#f5cf16',
            borderColor: '#f5cf16',
            fill: false,
          }
        ]
      }
    })
  }

  getUsers() {
    this.spinner.show();
    this.userService.getAllUsers(this.filters).subscribe((response) => {
      if (response.success) {
        this.users = response.users.length;
      } else {
        this.users = 0;
      }
      this.spinner.hide();
    });
  }





}
